<?php
class Recibos_alquileres_model  extends grocery_CRUD_Model  {

	protected $primary_key = null;
	protected $table_name = null;
	protected $relation = array();
	protected $relation_n_n = array();
	protected $primary_keys = array();
        
        function __construct()
        {
            parent::__construct();
        }
        
        function get_list()
        {
            $permiso = $this->db->get_where('permisos',array('user'=>$_SESSION['user'],'tipo'=>'alquiler'));
            $where = 'where ';
            $or = ' ';
            if($permiso->num_rows==0)
                $where .= ' codigo_propietario = -1';
            foreach($permiso->result() as $p)
            {                
                $where.= $or."codigo_propietario='".$p->edificio."'";
                if($p->contrato!='')$where.= " AND contrato='".$p->contrato."'";
                $or = " OR ";
            }
            $results = $this->db->query('select * from txt_inquilinos '.$where)->result();
            return $results;
        }
    
}
