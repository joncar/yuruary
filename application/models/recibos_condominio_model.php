<?php
class Recibos_condominio_model  extends grocery_CRUD_Model  {

	protected $primary_key = null;
	protected $table_name = null;
	protected $relation = array();
	protected $relation_n_n = array();
	protected $primary_keys = array();
        
        function __construct()
        {
            parent::__construct();
        }
        
        function get_list()
        {
            $permiso = $this->db->get_where('permisos',array('user'=>$_SESSION['user'],'tipo'=>'condominio'));
            $where = 'where';
            $or = ' ';                        
            foreach($permiso->result() as $p)
            {
                $where.= $or."edificio='".$p->edificio."'";
                if($p->unidad!='')$where.= " AND unidad='".$p->unidad."'";
                $or = " OR ";
            }
            $results = $permiso->num_rows>0 ? $this->db->query('select * from txt_propietarios_cond '.$where)->result():$this->db->get_where('txt_propietarios_cond',array('id'=>'-1'))->result();
            return $results;
        }
    
}
