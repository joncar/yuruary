<?php
class Public_model  extends grocery_CRUD_Model  {

	protected $primary_key = null;
	protected $table_name = null;
	protected $relation = array();
	protected $relation_n_n = array();
	protected $primary_keys = array();
        
        function __construct()
        {
            parent::__construct();
        }
        
        function db_update($post_array, $primary_key_value)
        {
            $primary_key_field = $this->get_primary_key();
            return $this->db->update($this->table_name,$post_array, array( $primary_key_field => $primary_key_value));
        }
}