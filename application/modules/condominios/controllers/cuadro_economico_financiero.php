<?php
require_once APPPATH.'/controllers/panel.php';    
class Cuadro_economico_financiero extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(empty($_SESSION['user']))
                header("Location:".base_url());
	}
         public function index()
        {
            $this->as = array('index'=>'txt_cuadro_economico');
            $crud = $this->crud_function('','');
            $crud->set_subject('Cuadros económicos financieros');
            $crud->where('edificio',$this->user->edificio);            
                
            //Fields
            
            //unsets
            
            $crud->unset_delete();
            $crud->unset_edit();
            $crud->unset_add();
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_read()
                 ->columns('edificio','fecha','mes','saldo','fondo','saldo_inmueble')
                 ->unset_searchs = array('edificio');
            $crud->add_action('<i class="fa fa-print"></i> Estado de cuenta','',base_url('condominios/cuadro_economico_financiero/imprimir_cuadro').'/');
            //Displays            
            //Fields types
            
            //Validations
            
            //Callbacks 
            $crud->callback_column('edificio',function($val,$row){
                $ed = get_instance()->db->get_where('txt_edificios',array('codigo'=>$val));
                if($ed->num_rows==0){
                    return 'No Válido';
                }else{
                    return $ed->row()->nombre;
                }
            });
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'estado_cuenta';
            $this->loadView($output);
        }
        
        function imprimir_cuadro($id)
        {
            if(empty($id))
            $this->load->view('404');
            else
            {
                $data = $this->db->get_where('txt_cuadro_economico',array('id'=>$id));
                if($data->num_rows>0){
                 $data = $data->row();                 
                 $edificio = $this->db->get_where('txt_edificios',array('codigo'=>$data->edificio));                                  
                 $this->load->view('reportes/cuadro_economico',array('recibo'=>$data,'edificio'=>$edificio->row()));   
                }
                else
                $this->load->view('404');
            }
        }
}

?>