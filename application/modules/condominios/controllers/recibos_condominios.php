<?php
require_once APPPATH.'/controllers/panel.php';    
class Recibos_condominios extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(empty($_SESSION['user']))
                header("Location:".base_url());
	}
        public function index()
        {
            $this->as = array('index'=>'txt_recibos_cond');
            $crud = $this->crud_function('','');
            $crud->where('edificio',$this->user->edificio);
            //$crud->where('unidad',$this->user->unidad);
            $crud->unset_delete();
            $crud->unset_edit();
            $crud->unset_add();
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_read();
            $crud->unset_searchs = array('edificio','conjunto');
            $crud->columns('nro_recibo','edificio','unidad','propietario','mes_ano','neto');
            //Displays
            $crud->display_as('mes_ano','Fecha');
            //Fields types
            $crud->add_action('<i class="fa fa-print"></i> Imprimir Recibo','',base_url('condominios/recibos_condominios/imprimir').'/');
            //Validations
            //Relations            
            //Callbacks
            $crud->callback_column('edificio',function($val,$row){
                $ed = get_instance()->db->get_where('txt_edificios',array('codigo'=>$val));
                if($ed->num_rows==0){
                    return 'No Válido';
                }else{
                    return $ed->row()->nombre;
                }
            });
            $crud->callback_column('unidad',function($val,$row){
                if(get_instance()->db->get_where('txt_recibos_cond',array('edificio'=>$row->edificio,'unidad'=>$val))->num_rows>0)
                return '<a href="'.base_url('recibos_condominios/recibos/'.$row->edificio.'/'.$val).'">'.$val.'</a>';
                else return $val;
            });
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'recibos';
            $this->loadView($output);
        }                
        
        function imprimir($id)
        {
            if(empty($id))
            $this->load->view('404');
            else
            {
                $data = $this->db->get_where('txt_recibos_cond',array('id'=>$id));
                if($data->num_rows>0){
                 $data = $data->row();
                 $detalle = $this->db->get_where('txt_recibos_cond_detalles',array('recibo'=>$data->id));                 
                 $edificio = $this->db->get_where('txt_edificios',array('codigo'=>$data->edificio));                 
                 $fondos = $this->db->get_where('fondos_condominio',array('recibo'=>$data->id));                 
                 $this->load->view('reportes/recibos_condominio',array('recibo'=>$data,'detalle'=>$detalle,'edificio'=>$edificio->row(),'fondos'=>$fondos));   
                }
                else
                $this->load->view('404');
            }
        }

}

?>