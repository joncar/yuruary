<?php
require_once APPPATH.'/controllers/panel.php';    
class Paginas extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('paginas'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $this->as = array('index'=>'paginas');
            $crud = $this->crud_function('','');
            $crud->set_subject('Paginas');
            //Fields
            $crud->edit_fields('titulo','texto','incrustar','incrustar2','incrustar3','incrustar4');
            //unsets
            $crud->unset_add();
            $crud->unset_delete();
            $crud->unset_columns('url','incrustar','incrustar2','incrustar3','incrustar4');
            //Displays
            $crud->display_as('texto','Contenido');
            
            //Fields types
            $crud->set_field_upload('incrustar','files');
            $crud->set_field_upload('incrustar2','files');
            $crud->set_field_upload('incrustar3','files');
            $crud->set_field_upload('incrustar4','files');
            //Validations
            $crud->required_fields('titulo','texto');
            
            //Callbacks
            $crud->callback_before_upload(array($this,'bupload'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'paginas';
            $this->loadView($output);
        }
        
        public function bupload($files_to_upload, $field_info)
        {
            $type = $files_to_upload[$field_info->encrypted_field_name]['type'];
            $image_info = getimagesize($files_to_upload[$field_info->encrypted_field_name]["tmp_name"]);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            
            if ($type != 'image/png' && $type!='image/jpg' && $type!='image/jpeg')
            {
                return 'Extension no permitida';
            }
            return true;
        }
}

?>