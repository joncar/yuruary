<?php
require_once APPPATH.'/controllers/panel.php';    
class Link extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('link'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $this->as = array('index'=>'links');
            $crud = $this->crud_function('','');
            $crud->set_subject('Links de interes');
            //Fields
            
            //unsets
            
            //Displays
            $crud->display_as('url','Url de la web');
            //Fields types
            $crud->set_field_upload('foto','files');
            //Validations
            $crud->required_fields('url','titulo');
            $crud->set_rules('url','Url','required|valid_url');
            //Callbacks
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'link';
            $this->loadView($output);
        }
}

?>