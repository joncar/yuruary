<?php
require_once APPPATH.'/controllers/panel.php';    
class Propietarios extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('tablas'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $this->as = array('index'=>'txt_propietarios');
            $crud = $this->crud_function('','');
            $crud->set_subject('Propietarios');
            //Fields
            $crud->edit_fields('denominacion','direccion');
            $crud->columns('codigo','denominacion','inquilinos');
            //unsets
            $crud->unset_add();
            //Displays
            $crud->display_as('codigo','Cuenta Nº')
                 ->display_as('denominacion','Señor(es)');
            //Fields types
            
            //Validations
            $crud->required_fields('denominacion','direccion');
            //Callbacks
            $crud->callback_column('inquilinos',array($this,'__inquilinos'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'tablas';
            $this->loadView($output);
        }
        
        public function __inquilinos($val,$row)
        {
            $in = $this->db->get_where('txt_inquilinos',array('codigo_propietario'=>$row->codigo));
            if($in->num_rows>0)
                return '<a href="'.base_url('propietarios/inquilinos/'.$row->codigo).'">'.$in->num_rows.' inquilinos</a>';
            return 'No posee ';
        }
        
        public function inquilinos($id = '')
        {
            if($this->db->get_where('txt_propietarios',array('codigo'=>$id))->num_rows>0)
            {
                $_SESSION['propietario_txt'] = $id;
                header("Location:".base_url('propietarios/show_inquilinos'));
            }
            else
                header("Location:".base_url('propietarios'));
        }
        
        public function show_inquilinos()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('txt_inquilinos');
            $crud->set_subject('inquilino');
            $crud->where('codigo_propietario',$_SESSION['propietario_txt']);
            //Fields
            
            
            //unsets
            $crud->unset_add();
            //Displays
            
            //Fields types
            
            //Validations
            
            //Callbacks
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'inquilinos';
            $this->loadView($output);
        }
}

?>