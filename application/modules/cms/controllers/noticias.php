<?php
require_once APPPATH.'/controllers/panel.php';    
class Noticias extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('noticias'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $this->as = array('index'=>'noticias');
            $crud = $this->crud_function('','');
            $crud->set_subject('Noticias');
            
            //Fields
            
            //unsets
            
            //Displays
            $crud->display_as('texto','Contenido que se va a publicar');
            //Fields types
            $crud->set_field_upload('imagen','files');
            //Validations
            
            //Callbacks
            $crud->callback_before_insert(array($this,'addfecha'));
            $crud->callback_before_upload(array($this,'bupload'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'usuarios';
            $this->loadView($output);
        }
        
        public function bupload($files_to_upload, $field_info)
        {
            $type = $files_to_upload[$field_info->encrypted_field_name]['type'];
            $image_info = getimagesize($files_to_upload[$field_info->encrypted_field_name]["tmp_name"]);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            
            if ($type != 'image/png' && $type!='image/jpg' && $type!='image/jpeg')
            {
                return 'Extension no permitida';
            }
            else
            {
                if($image_width!=120 || $image_height!=120)
                return 'Dimensiones de imagen no soportadas, La imagen debe ser de 120x120';
                else
                return true;
            }
            
        }
}

?>