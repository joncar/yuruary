<?php
require_once APPPATH.'/controllers/panel.php';    
class Actualizar extends Panel{
 	public function __construct()
	{
            parent::__construct();
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('user');
            $crud->set_subject($_SESSION['nombre']);
            $crud->set_model('public_model');
            $crud->where('id',$_SESSION['user']);
            //Fields
            
            //unsets
            $crud->unset_delete();
            $crud->unset_add();
            $crud->unset_export();
            $crud->unset_print();
            $crud->unset_read();
            $crud->unset_columns('password','tipo','status','unidades');
            $crud->unset_fields('tipo','status','unidades','usuario','fecha');
            //Displays
            $crud->display_as('dir_hab','Dirección de habitación');
            //Fields types
            $crud->field_type('password','password');
            $crud->field_type('email','readonly');
            //Validations
            $crud->required_fields('nombre','apellido','telefonos');
            $crud->set_rules('telefono','required|alpha_numeric');
            $crud->set_rules('celular','alpha_numeric');
            //Callbacks
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'usuarios';
            $this->loadView($output);
        }
}

?>