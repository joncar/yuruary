<article class="row">
    <? if(!empty($crud)): ?>
    <div class="col-lg-3"><? $this->load->view('includes/nav-panel') ?></div>
    <div class="col-lg-9">
    <? else: ?>
    <div class="col-lg-12">
    <? endif ?>
        <? if(empty($crud)): ?>
        <?php if($this->db->get_where('permisos',array('tipo'=>'alquiler','user'=>$_SESSION['user']))->num_rows>0): ?>
        <h3 class="dividir">Alquileres</h3>
        <div class="row" align="center">
            <a href="<?= base_url('recibos') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-inbox"></i><br/> Recibos</a>
            <a href="<?= base_url('estado_cuenta') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-usd"></i><br/> Estado de cuenta</a>
            <a href="<?= base_url('deudor') ?>" class="col-lg-2 well"><i class="glyphicon  glyphicon glyphicon-time"></i><br/> Deudor</a>
        </div>
        <?php endif ?>
        <?php if($this->db->get_where('permisos',array('tipo'=>'condominio','user'=>$_SESSION['user']))->num_rows>0): ?>
        <h3 class="dividir">Condominios</h3>
        <div class="row" align="center">
                <a href="<?= base_url('recibos_condominios') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-inbox"></i><br/> Recibos</a>
        </div>
        <?php endif ?>
        <? if($_SESSION['cuenta'] == 99): ?>
        <h3 class="dividir">Administración</h3>
        <div class="row" align="center">
                <?if($this->querys->getAccess('usuarios')): ?><a href="<?= base_url('usuarios') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-user"></i><br/> Usuarios</a><? endif ?>
                <?if($this->querys->getAccess('paginas')): ?><a href="<?= base_url('paginas') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-list-alt"></i><br/> Paginas</a><? endif ?>
                <?if($this->querys->getAccess('link')): ?><a href="<?= base_url('link') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-link"></i><br/> Links de interes</a><? endif ?>
                <?if($this->querys->getAccess('leyes')): ?><a href="<?= base_url('leyes') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-tower"></i><br/> Leyes</a><? endif ?>
                <?if($this->querys->getAccess('boletines')): ?><a href="<?= base_url('boletines') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-envelope"></i><br/> Boletines</a><? endif ?>
                <?if($this->querys->getAccess('inmuebles')): ?><a href="<?= base_url('inmuebles') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-bookmark"></i><br/> Inmuebles</a><? endif ?>
                <?if($this->querys->getAccess('token')): ?><a href="<?= base_url('token') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-lock"></i><br/> Token</a><? endif ?>
                <?if($this->querys->getAccess('noticias')): ?><a href="<?= base_url('noticias') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-calendar"></i><br/> Noticias</a><? endif ?>
                <?if($this->querys->getAccess('ajustes')): ?><a href="<?= base_url('ajustes') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-wrench"></i><br/> Ajustes</a><? endif ?>
                <?if($this->querys->getAccess('banner')): ?><a href="<?= base_url('banner') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-picture"></i><br/> Banner</a><? endif ?>
                <?if($this->querys->getAccess('importar')): ?><a href="<?= base_url('importar') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-import"></i><br/> Importar</a><? endif ?>
                
                
        </div>
        <h3 class="dividir">Informes Económicos </h3>
        <div class="row" align="center">
            <a href="<?= base_url('reportes/cuadro') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-file"></i><br/> Cuadro economico financiero</a>
            <a href="<?= base_url('reportes/facturacion') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-file"></i><br/> Facturacion de condominio por unidad</a>
            <a href="<?= base_url('reportes/facturacion') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-file"></i><br/> Facturacion de condominio por inmueble</a>
        </div>
        <?if($this->querys->getAccess('tablas')): ?>
        <h3 class="dividir">Tablas</h3>
        <div class="row" align="center">
                <a href="<?= base_url('tipos') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-list-alt"></i><br/> Tipos de inmuebles </a>
                <a href="<?= base_url('operaciones') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-list-alt"></i><br/> Operaciones</a>
                <a href="<?= base_url('propietarios') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-list-alt"></i><br/> Propietarios</a>
                <a href="<?= base_url('permisos') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-lock"></i><br/> Permisos</a>
        </div>
        <? endif ?>
        <? endif ?>
        <center><a href="<?= base_url('main/unlog') ?>" class="col-lg-2 well"><i class="glyphicon glyphicon-remove"></i><br/> Cerrar Sessión </a></center>
        <? else: ?>
        <? $this->load->view('cruds/'.$crud) ?>
        <? endif ?>
    </div>
</article>