    <div class="thumbnail">
      <?= img($img,'width:30%') ?>
      <div class="caption">
        <b><?= $title ?></b>
        <?= $content ?>
      </div>
    </div>
