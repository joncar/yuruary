<!Doctype html>
<html lang="es">
    <head>
        <meta charset='utf-8'>
        <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
        
        <style>
            body{font-size:11px; margin-left:24px;}
        </style>
    </head>
    <body>                       		                       
		<section class="container row">
		  <div class="col-xs-5">
		    <img style="width:100%" src="<?= base_url('img/logo.jpg') ?>"><br/><br/><br/>
                    <div align="center">AVISO DE COBRO - WEB</div>
		  </div>
		  <div style="" class="col-xs-7">
		    <table style="width:100%" border="1">      
			<tbody><tr>
			  		<th class="col-xs-10" colspan="2" style="text-align:center">INMUEBLE</th>
			  		<th style="text-align:center">UNIDAD</th>
			</tr>
		      	<tr>
			  		<td class="col-xs-10" colspan="2" style="text-align:left"><?= $recibo->edificio.' '.$edificio->nombre ?></td>
			  		<td style="text-align:center"><?= $recibo->unidad ?></td>
			</tr>
			<tr>
			  <th style="text-align:center" colspan="3">PROPIETARIO</th>
		      	</tr>
		      	<tr>
			  <td style="text-align:left" colspan="3"><?= $recibo->propietario ?></td>
		      	</tr>
		      	<tr>
			  <th style="text-align:center">MES/AñO</th>
			  <th style="text-align:center">ALICUOTA %</th>
			  <th style="text-align:center">NETO</th>
		      	</tr>
			<tr>
			  <td style="text-align:center"><?= $recibo->mes_ano ?></td>
			  <td style="text-align:center"><?= $recibo->alicuota ?></td>
			  <td style="text-align:center"><?= $recibo->neto ?></td>
		      	</tr>
		    </tbody></table>
		  </div>
		</section>
		<section style="margin-right:0px; margin-left:0" class="container row">
		  <table style="width:97.5%" border="1">
		    <tbody><tr>	
		      <th style="text-align:center" class="col-xs-7" valign="top">CONCEPTO DE PAGO</th>
		      <th style="text-align:center" class="col-xs-1" valign="top">CONJUNTO</th>
		      <th style="text-align:center" class="col-xs-1" valign="top">INMUEBLE</th>
		      <th style="text-align:center" class="col-xs-1" valign="top">Bs./UNIDAD</th>
		    </tr>
		    <tr>	
		      <td class="col-xs-7" style="height: 360px; text-align:left" valign="top">
                          <? foreach($detalle->result() as $r): ?>
                          <?= $r->detalle.'<br/>' ?>
                          <? endforeach ?>
                      </td>
		      <td style="text-align:right" class="col-xs-1" valign="top">
                          <? foreach($detalle->result() as $r): ?>
                          <?= $r->conjunto.'<br/>' ?>
                          <? endforeach ?>
                      </td>
		      <td style="text-align:right" class="col-xs-1" valign="top">
                          <? foreach($detalle->result() as $r): ?>
                          <?= $r->inmueble.'<br/>' ?>
                          <? endforeach ?>
                      </td>
		      <td style="text-align:right" class="col-xs-1" valign="top">
                          <? foreach($detalle->result() as $r): ?>
                          <?= $r->unidad.'<br/>' ?>
                          <? endforeach ?>
                      </td>
		    </tr>
		<tr>	
		    <td style="text-align:right" class="col-xs-6" valign="top">TOTAL GASTOS DEL MES</td>
		      <td style="text-align:right" class="col-xs-2" valign="top"><?= $recibo->total_conjunto ?></td>
		      <td style="text-align:right" class="col-xs-2" valign="top"><?= $recibo->total_inmueble ?></td>
		      <td style="text-align:right" class="col-xs-2" valign="top"><?= $recibo->total_unidad ?></td>
		    </tr>
		 <tr>	
		    <td style="text-align:left" class="col-xs-6" valign="top"><?= $recibo->observaciones ?></td>
		      <td valign="top" colspan="3" class="col-xs-2" style="height:60px">
                        <div class="row">
                          <div class="col-xs-6" style="text-align:center; margin-top:40px;">
                            _________________<br/>Firma autorizada
                          </div>
                          <div class="col-xs-6" style="text-align:center; margin-top:40px;">
                            _________________<br/>Firma cobrador
                          </div>
                        </div>
                      </td>
		    </tr>
		  </tbody></table>
		</section>
	<section style="margin-right:0px; margin-left:0" class="container row">
		<div class="col-xs-5" style="padding-left:20px">
	  IDENTIFICACION DEL CLIENTE: <?= $recibo->identificacion_cliente ?>	  
	  </div>
	  <div class="col-xs-7">NUMERO DE RECIBO: <?= $recibo->nro_recibo ?></div>
    <br>
	  <div class="col-xs-12" style="padding-left:20px">
	    Su deuda a la fecha incluyendo la presente es de <span style="border:1px solid black;">&nbsp; &nbsp; &nbsp; <?= $recibo->pendientes ?> &nbsp; &nbsp; &nbsp; &nbsp;</span> mes(es) y suman Bs <span style="border:1px solid black; padding:0 3px 0 20px"><?= $recibo->total_deuda ?></span>
	  </div>
	</section>
	<section style="margin-top:2px; margin-right:0px; margin-left:0" class="container row">
	  <table style="width:100%" border="1px">
	    <tbody><tr>
	      <td align="center">Fondos</td>
	      <td align="center">Saldo Anterior</td>
	      <td align="center">Cuota del Mes</td>
	      <td align="center">Cargo/Abono</td>
	      <td align="center">Saldo Actual</td>
	    </tr>
            <?php for($i=0;$i<4;$i++): ?>
	    <tr>
	      <td align="center"><?= $i<$fondos->num_rows?$fondos->row($i)->fondos:'&nbsp' ?></td>
	      <td align="center"><?= $i<$fondos->num_rows?$fondos->row($i)->saldo_anterior:'&nbsp' ?></td>
	      <td align="center"><?= $i<$fondos->num_rows?$fondos->row($i)->cuota_mes:'&nbsp' ?></td>
	      <td align="center"><?= $i<$fondos->num_rows?$fondos->row($i)->cargoabono:'&nbsp' ?></td>
	      <td align="center"><?= $i<$fondos->num_rows?$fondos->row($i)->saldo_actual:'&nbsp' ?></td>
	    </tr>
	    <?php endfor ?>
	  </tbody></table>
		</section> 
	<section style="margin-top:2px; margin-right:0px; margin-left:0" class="container row">
	  <table style="width:100%" border="1px">
	    <tbody><tr>
	      <th align="center">INMUEBLE</th>
	      <th align="center">UNIDAD
	      </th><th align="center">DATOS DE COBRO</th>      
	    </tr>
	  	<tr>
	      <td align="center"><?= $edificio->nombre ?></td>
	      <td align="center"><?= $recibo->unidad ?></td>
	      <td align="center"></td>      
	    </tr>
	  	<tr>
	      <th align="center">PROPIETARIO</th>
	      <th align="center">MONTO      
	    </th></tr>
	      <tr>
	      <td align="center"><?= $recibo->propietario ?></td>
	      <td align="center"><?= $recibo->total_unidad ?>      
	    </td></tr>
	  </tbody></table>
		</section> 
        
        <section class="container row" style="margin-top:20px; margin-right:0px; margin-left:0">
	  <table border="1px" style="width:100%">
	    <tbody><tr>
	      <th align="center">ID. DE CLIENTE</th>
	      <th align="center">VENCIMIENTO</th>
              <th align="center">NRO DE RECIBO</th>
              <th align="center">MESES PENDIENTES</th>
              <th align="center">TOTAL DE LA DEUDA</th>
	    </tr>
	  	<tr>
	      <td align="center"><?= $recibo->identificacion_cliente ?></td>
	      <td align="center"><?= date("d/m/Y",strtotime($recibo->vencimiento)) ?></td>
	      <td align="center"><?= $recibo->nro_recibo ?></td>      
              <td align="center"><?= $recibo->pendientes ?></td>      
              <td align="center"><?= $recibo->total_deuda ?></td>      
	    </tr>	  	
	  </tbody></table>
          COMBO DE SERVICIO PROVINCIAL 649
        </section>
        <script>
            window.print();
        </script>
    </body>
</html>

