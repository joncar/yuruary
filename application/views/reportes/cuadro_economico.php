<!Doctype html>
<html lang="es">
    <head>
        <meta charset='utf-8'>
        <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
        
        <style>
            body{font-size:11px; margin-left:24px;}
        </style>
    </head>
    <body>        
        <div class="row">
            <div class="col-xs-6">
              Administradora Yuruary CA <br>
              YUR004 /CGONZALEZ
            </div>
            <div class="col-xs-5" align="right">
              Pagina: 107 <br>
              Fecha: <?= date("d/m/y",strtotime($recibo->fecha)) ?> <br>
              Hora..: <?= $recibo->hora ?>
            </div>
          </div>
          <div align="center">
            <b>Cuadro Economico Financiero</b>
          </div>
          <div class="row">
                  Conjunto: <?= $recibo->conjunto ?> <?= $recibo->conjunto_nombre ?>
          </div>
          <div class="row">
                  Inmueble: <?= $recibo->edificio ?> <?= $edificio->nombre ?>
          </div>
          <div class="row">
            al Mes: <?= $recibo->mes ?>
          </div>
          <div class="row">
            <div class="col-xs-4 col-xs-offset-2">
              Saldo de Fondos al mes <?= $recibo->mes ?>
            </div>
            <div class="col-xs-3" align="right">
              <?= number_format($recibo->saldo,2,',','.') ?>CR
            </div>
          </div>
          <div class="row">
            <div class="col-xs-4 col-xs-offset-2">
              Deuda Condominio al Mes <?= $recibo->mes ?>
            </div>
            <div class="col-xs-3" align="right">
              <?= number_format($recibo->deuda,2,',','.') ?>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-4 col-xs-offset-2">
              Fondo de Operacion
            </div>
            <div class="col-xs-3" align="right">
              <?= number_format($recibo->fondo,2,',','.') ?>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-4 col-xs-offset-2">
              Pagos contra los Fondos
            </div>
            <div class="col-xs-3" align="right">
              <?= number_format($recibo->pagos,2,',','.') ?>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-4 col-xs-offset-2">
              Saldo del Inmueble
            </div>
            <div class="col-xs-3" align="right" style="border-top:1px dashed black">
              <?= number_format($recibo->saldo_inmueble,2,',','.') ?>CR
            </div>
          </div>
          <div class="row">
            <div class="col-xs-4 col-xs-offset-2">
              Rendimiento
            </div>
            <div class="col-xs-3" align="right">
              <?= number_format($recibo->rendimiento,2,',','.') ?>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-4 col-xs-offset-2">
              Financiamiento
            </div>
            <div class="col-xs-3" align="right">
              <?= number_format($recibo->financiamiento,2,',','.') ?>
            </div>
          </div>
        <script>
            window.print();
        </script>
    </body>
</html>

