<!Doctype html>
<html lang="es">
    <head>
        <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
        
        <style>
            body{font-size:12px; margin-left:24px;}
        </style>
    </head>
    <body>
        
        
        <section class="container row">
            <div class="col-xs-5">
                <img src="http://www.yuruary.com.ve/img/logo.jpg" style="width:100%">
            </div>
            <div class="col-xs-7" style='text-align:right'>
                Fecha: <?= date("d/m/Y") ?><br/>
                Hora: <?= date("H:i:s") ?>
            </div>
        </section>
        
        <section class="container" style="text-align:center; font-weight: bold">
            <h4><b>DEUDOR DE ALQUILERES - WEB</b></h4>
        </section>
        <table style="width:100%">
            <thead>
                <tr style='border:1px solid black'>
                    <th>PROPIETARIO</th>
                    <th>PERIODO</th>
                    <th>MONTO</th>
                    <th>MTO. ACUM</th>
                </tr>
            </thead>
            <tbody>
                <? $edificio = ''; $recibos = 0; $recibos_totales = 0; $total = 0; ?>
                <? foreach($deudor->result() as $d): ?>
                    <? $inquilino = $this->db->get_where('txt_inquilinos',array('contrato'=>$d->contrato))->row() ?>
                    <? if($edificio!=$d->inmueble): ?>
                    <tr>
                        <td colspan="4"><b><?= $inquilino->codigo_propietario ?> <?= $d->inmueble ?></b></td>
                    </tr>
                    <? $edificio = $d->inmueble; $saldo = 0; ?>
                    <? endif ?>
                    <? $recibos++ ?>
                    <tr>
                        <td>
                            <?= $inquilino->codigo_unidad ?>
                            <?= $inquilino->codigo ?>
                            <?= $inquilino->nombre ?>
                        </td>
                        <td align='right'><?= $d->mes_emision."-".$d->ano_emision ?></td>
                        <td align='right'><?= $d->monto ?><? $saldo+=$d->monto ?></td>
                        <td align='right'><?= $saldo ?></td>
                    </tr>
                    <!--Total apartamento-->
                    <? if($this->db->get_where('txt_deudores',array('contrato'=>$d->contrato,'unidad'=>$d->unidad))->num_rows == $recibos): ?>
                    <tr>
                        <td colspan="1" align='right'><b>Total apartamento: (<?= $inquilino->codigo_unidad ?>) Recibos: <?= $recibos ?></b></td>
                        <td>&nbsp</td>
                        <td align='right' style='border-top:1px solid black'><b><?= $saldo ?><? $total+= $saldo; $saldo = 0; $recibos_totales+= $recibos; $recibos = 0; ?></b></td>
                    </tr>
                    <? endif?>
                
                  <!-- Total edificio -->
                  <? if($this->db->get_where('txt_deudores',array('contrato'=>$d->contrato,'inmueble'=>$d->inmueble))->num_rows == $recibos_totales): ?>
                  <tr><td colspan='4'>&nbsp</td></tr>
                  <tr>
                      <td colspan='1' align='right'><b>Total (<?= $inquilino->codigo_propietario ?>) <?= $d->inmueble ?> Recibos: <?= $recibos_totales ?></b></td>
                      <td>&nbsp</td>
                      <td align='right'><b><?= $total ?><? $total = 0; $recibos_totales = 0; ?></b></td>
                  </tr>
                  <? endif?>
                  <? endforeach ?>
            </tbody>
        </table>
            <center><b>Copia del original</b></center>
        <script>
            window.print();
        </script>
    </body>
</html>
