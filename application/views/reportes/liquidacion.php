<!Doctype html>
<html lang="es">
    <head>
        <meta charset='utf-8'>
        <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
        
        <style>
            body{font-size:12px; margin-left:24px;}
        </style>
    </head>
    <body>
        
        
        <section class="container row">
            <div class="col-xs-5">
                <img src="http://www.yuruary.com.ve/img/logo.jpg" style="width:100%">
            </div>
            <div class="col-xs-7">
                <h4 style="text-align:center"><b>LIQUIDACION DE PROPIETARIO(S)</b><br/>*** <?= meses($liquidacion->mes_cuenta)." ".$liquidacion->ano_cuenta ?> ***</h4>
            </div>
        </section>
        
        <section class="container" style="text-align:center; font-weight: bold">
            <h4><b><?= $this->db->get_where('txt_propietarios',array('codigo'=>$liquidacion->codigo_propietario))->row()->denominacion." (".$liquidacion->codigo_propietario.")" ?><br/>
            <?= $liquidacion->denominacion ?><br/>
            MONTO DEL ESTADO DE CUENTAS Bs. <?= number_format($liquidacion->monto,2,'.',',') ?> *</b></h4>
        </section>
        <section class="container row" style="border:1px solid black">
            <div class="col-xs-6"><b>PROPIETARIO(S)</b></div>
            <div class="col-xs-2"><b>DEBE</b></div>
            <div class="col-xs-2"><b>HABER</b></div>
            <div class="col-xs-2"><b>SALDO</b></div>
        </section>
        <? foreach($detalle->result() as $d): ?>
        <section class="container row">
            <div class="col-xs-6"><?= ($d->debe==0 && $d->haber==0 && $d->saldo==0)?'<b><u>'.$d->concepto.'</u></b>':$d->concepto ?></div>
            <div class="col-xs-2" align='right'><?= $d->debe!=0?number_format($d->debe,2,'.',','):'' ?></div>
            <div class="col-xs-2" align='right'><?= $d->haber!=0?number_format($d->haber,2,'.',','):'' ?></div>
            <div class="col-xs-2" align='right'><?= $d->saldo!=0?number_format($d->saldo,2,'.',',')." *":'' ?> </div>
        </section>
        <? endforeach ?>
        <center><b>Copia Web <br/>Fecha de impresión: <?= date("d/m/Y H:i:s") ?></b></center>
        <script>
            window.print();
        </script>
    </body>
</html>
