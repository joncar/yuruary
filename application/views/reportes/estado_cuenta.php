<!Doctype html>
<html lang="es">
    <head>
        <meta charset='utf-8'>
        <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
        
        <style>
            body{font-size:12px;}
        </style>
    </head>
    <body>
        
        
        <section class="container row">
            <div class="col-xs-5">
                <img src="http://www.yuruary.com.ve/img/logo.jpg" style="width:100%">
                <div style="font-size:10px; text-align: center">
                    TORRE CANAIMA - P.B. - LOCALES 2 Y 3 - AV. ESTE 2
                    LOS CAOBOS - TLF. 573-0122 MASTER - 572.7537 FAX
                    APARTADO 4229 - CARMELITAS - CARACAS 1010-A
                    RIF. J-00113810-2 NIT: 0030662075<br/>
                    Página Web: www.yuruary.com<br/>
                    Correo Electrónico: alquileres@yuruary.com<br/>
                </div>
            </div>
            <div class="col-xs-7">
                <h4 style="text-align:center"><b>ESTADO DE CUENTA</b></h4>
                <div class="row">
                    <div class="col-xs-12" style="border:1px solid black; border-bottom:0px" align="center"><b>INMUEBLE ADMINISTRADO</b></div>
                    <div class="col-xs-12" style="border:1px solid black; min-height:40px"><b><?= $estado->denominacion ?></b> <br/> <?= $estado->direccion ?></div>
                </div>
            </div>
        </section>
    

        <section class="container row">
            <div class="col-xs-5" style="border:1px solid black; min-height:123px">
                SEÑOR(ES)
                <p> <b><?= $this->db->get_where('txt_propietarios',array('codigo'=>$estado->codigo_propietario))->row()->denominacion ?></b></p>
                <p><?= $estado->direccion ?></p>
            </div>
            <div class="col-xs-7">
                <div class="row col-xs-7">
                    <div class="col-xs-12" style="border:1px solid black; border-bottom:0px" align="center"><b>Forma de pago</b></div>
                    <div class="col-xs-12" style="border:1px solid black; min-height:40px"><?= $estado->formas_pagos ?></div>
                </div>
                <div class="row col-xs-6">
                    <div class="col-xs-12" style="border:1px solid black; border-bottom:0px" align="center"><b>MES</b></div>
                    <div class="col-xs-12" style="border:1px solid black; min-height:40px"><?= meses($estado->mes_cuenta)." ".$estado->ano_cuenta ?></div>
                </div>
                
                <div class="row col-xs-6">
                    <div class="col-xs-12" style="border:1px solid black; border-bottom:0px" align="center"><b>CUENTA NRO.</b></div>
                    <div class="col-xs-12" style="border:1px solid black; min-height:40px"><?= $estado->codigo_propietario ?></div>
                </div>
                <div class="row col-xs-7">
                    <div class="col-xs-12" style="border:1px solid black; border-bottom:0px" align="center"><b>GASTOS DE ADMINISTRACIÓN</b></div>
                    <div class="col-xs-12" style="border:1px solid black; min-height:40px"><?= "****".$estado->porcentaje." %****" ?></div>
                </div>
            </div>
        </section>
        
        <section class="container row" style="border:1px solid black" >
            <div class="row" style="border:1px solid black">
                <div class="col-xs-1"><b>DIA</b></div>
                <div class="col-xs-5"><b>CONCEPTO</b></div>
                <div class="col-xs-2"><b>DEBE</b></div>
                <div class="col-xs-2"><b>HABER</b></div>
                <div class="col-xs-2"><b>SALDO</b></div>
            </div>
            <? foreach($detalle->result() as $d): ?>
            <? $saldo = $d->saldo ?>
            <div class="row">
                <div class="col-xs-1"><b><?= $d->dia ?></b></div>
                <div class="col-xs-5"><b><?= $d->concepto ?></b></div>
                <div class="col-xs-2" align='right'><b><?= $d->debe ?></b></div>
                <div class="col-xs-2" align='right'><b><?= $d->haber ?></b></div>
                <div class="col-xs-2" align='right'><b><?= $saldo ?></b></div>
            </div>
            <? endforeach ?>
        </section>
        
        <section class="container row">
            <div class="col-xs-12 row">
            <div class="col-xs-6"><h1>WEB</h1></div>
            <div class="col-xs-6 row">
                <div class="row"><div class="col-xs-4" style="border:1px solid black;"><b>CUENTA N°.</b></div>
                <div class="col-xs-4" style="border:1px solid black;"><b>PERIODO</b></div>
                <div class="col-xs-4" align='right' style="border:1px solid black;"><b>SALDO</b></div></div>
                <div class="row"><div class="col-xs-4" style="border:1px solid black;"><?= $estado->codigo_propietario ?></div>
                <div class="col-xs-4" style="border:1px solid black;"><?= meses($estado->mes_cuenta)." ".$estado->ano_cuenta ?></div>
                <div class="col-xs-4" align='right' style="border:1px solid black;"><?= $saldo ?></div></div>
            </div>
            </div>
            <div class="col-xs-12 row">
                <b>NOTA: 
                    <ul>
                        <li>LOS SALDOS CON SIGNO * SON A NUESTRO FAVOR Y CON SIGNO CR A FAVOR DEL PROPIETARIO.</li>
                        <li>LOS GASTOS DE ADMINISTRACION INCLUYEN I.V.A.</li>
                    </ul></b>
            </div>
            <center><b>Copia Web <br/>Fecha de impresión: <?= date("d/m/Y H:i:s") ?></b></center>
        </section>
        <script>
            window.print();
        </script>
    </body>
</html>
