<!Doctype html>
<html lang="es">
    <head>
        <meta charset='utf-8'>
        <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url('http://www.yuruary.com.ve/css/style.css') ?>">
        <style>
            body{font-size:12px;}
        </style>
    </head>
    <body>
        
        
        <section class="container row">
            <div class="col-xs-5">
                <img src="http://www.yuruary.com.ve/img/logo.jpg" style="width:100%">
                <div style="font-size:10px; text-align: center">
                    TORRE CANAIMA - P.B. - LOCALES 2 Y 3 - AV. ESTE 2
                    LOS CAOBOS - TLF. 573-0122 MASTER - 572.7537 FAX
                    APARTADO 4229 - CARMELITAS - CARACAS 1010-A
                    RIF. J-00113810-2 NIT: 0030662075<br/>
                    Página Web: www.yuruary.com<br/>
                    Correo Electrónico: alquileres@yuruary.com<br/>
                </div>
            </div>
            <div class="col-xs-7">
                <h4 style="text-align:center"><b>RECIBO DE ALQUILER - WEB</b></h4>
                <div class="row">
                    <div class="col-xs-12" style="border:1px solid black; border-bottom:0px" align="center"><b>INQUILINO</b></div>
                    <div class="col-xs-12" style="border:1px solid black; min-height:40px"><b><?= $inquilino->nombre ?> Contrato: <?= $inquilino->contrato ?></b></div>
                </div>
            </div>
        </section>
    

        <section class="container row">
            <div class="col-xs-5" style="border:1px solid black; min-height:123px">
                <b>DIRECCION DEL INMUEBLE</b>
                <p> <?= $recibo->direccion ?></p>
            </div>
            <div class="col-xs-7">
                <div class="row">
                    <div class="col-xs-12" style="border:1px solid black; border-bottom:0px" align="center"><b>INMUEBLE</b></div>
                    <div class="col-xs-12" style="border:1px solid black; min-height:40px"><?= $recibo->inmueble ?></div>
                    <div class="col-xs-12" style="border:1px solid black; border-bottom:0px" align="center"><b>UNIDAD</b></div>
                    <div class="col-xs-12" style="border:1px solid black; min-height:40px"><?= $recibo->unidad ?></div>
                </div>
            </div>
        </section>
        
        <section class="container row">
            <table style="width:100%" border="1">
                <thead>
                  <tr>
                  <th style="text-align:center">CODIGO INQUILINO</th>
                  <th style="text-align:center">CODIGO PROPIETARIO</th>
                  <th style="text-align:center">MES</th>
                  <th style="text-align:center">AÑO</th>
                  </tr>
                </thead>
                <tbody>
                    <? $meses = array('ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE') ?>
                  <tr>
                  <td style="text-align:center"><?= $inquilino->codigo ?></td>
                  <td style="text-align:center"><?= $inquilino->codigo_propietario ?></td>
                  <td style="text-align:center"><?= $meses[$recibo->mes_emision-1] ?></td>
                  <td style="text-align:center"><?= $recibo->ano_emision ?></td>
                  
                  </tr>
                  <tr>
                    <th style="text-align:center" colspan="3">CONCEPTO DE PAGO</th>
                    <th style="text-align:center" colspan="2">MONTO Bs.F.</th>
                  </tr>
                  <tr>
                    <td colspan="3">
                        <? foreach($detalle->result() as $d): ?>
                        <?= $d->denominacion_concepto ?><br/>
                        <? endforeach ?>
                    </td>
                    <td colspan="2" style="text-align:right">
                        <? foreach($detalle->result() as $d): ?>
                        <?= $d->monto ?><br/>
                        <? endforeach ?>
                    </td>
                  </tr>
</tbody></table>
<table ;="" style="width:100%" border="1">
                  <tbody><tr>
                        <th><center>DPTO. DE COBRANZA</center></th>
                        <th><center>COBRADOR</center></th>
                        <th><center>TOTAL A PAGAR</center></th>
                  </tr>
                  <tr style="height:50px">
                        <td></td>
                        <td></td>
                        <th>
                            <div class="row">
                            <div class="col-xs-2">Bs.F</div><div class="col-xs-10" align="right"> <?= number_format($recibo->monto,2,',','.') ?></div>
                            </div>
                            <div class="row">
                            <div class="col-xs-2">Bs.</div><div class="col-xs-10" align="right"> <?= number_format($recibo->monto*1000,2,',','.') ?></div>
                            </div>
                        </th>
                  </tr>
                 <tr>
                    <td><b>COBRANZA A DOMICILIO: Bs.F.</b> <?= $recibo->monto_domicilio ?></td>
                    <td colspan="2"><b>LUGAR Y FECHA DE COBRANZA:.</b> </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:center"><h5>ESTE RECIBO NO SERA VALIDO SIN LA FIRMA DEL DPTO. DE COBRANZAS Y DEL COBRADOR</h5></td>
                </tr>
                </tbody>
                
            </table>
            
            <small>
<ol style="font-size:10px">
    <li>ESTE RECIBO DEBE SER CANCELADO DENTRO DE LOS CINCO (5) PRIMEROS DIAS DEL MES.</li>
    <li>ROGAMOS A LOS ARRENDATARIOS OBSERVAR LAS CLAUSULAS DE SU CONTRATO CON ESTA ADMINISTRACION.</li>
    <li>EL PAGO DE ESTE RECIBO NO EXIME LA OBLIGACION DE CANCELAR OTRO U OTROS DE MESES ANTERIORES QUE TENGA PENDIENTE EL INQUILINO.</li>
    <li>EL PAGO QUE COMPRUEBA ESTE INSTRUMENTO LO RECIBIMOS POR CUENTA DEL PROPIETARIO DEL INMUEBLE.</li>
    <li>COMPROBANTE EMITIDO SIN ENMIENDAS NI RASPADURAS.</li>
    <li>NINGUN PAGO MEDIANTE CHEQUE SERA VALIDO, MIENTRAS DICHO CHEQUE NO HAYA SIDO EFECTIVAMENTE ACREDITADO EN LA CUENTA DE LA
ADMINISTRADORA, Y PARA EL SUPUESTO CASO QUE POR ERROR DICHO PAGO NO FUERE ACREDITADO EN LA CUENTA DE LA ADMINISTRADORA, ESTE SOLO
QUEDARA RESPONSABILIZADA CON EL DEUDOR HASTA EL MONTO DEL PAGO.</li>
</ol>
</small>
            <p align="center">Fecha de impresión: <?= date("d/m/Y H:i:s") ?></p>
            <div style="border:1px solid black; padding:5px; margin-bottom:20px"><div style="border:1px solid black; text-align:center; font-weight:bold;">***** RECUERDE QUE NUESTROS COBRADORES NO RECIBEN EFECTIVO ***** </div></div>
            <div style="border-top:1px dashed black; padding-top:20px;">
                <table border="1" style="width:100%; text-align:center">
                    <tr>
                        <th>CODIGO INQUILINO</th>
                        <th>CODIGO PROPIETARIO</th>
                        <th>UNIDAD</th>
                    </tr>
                    <tr>
                        <td><?= $inquilino->codigo ?></td>
                        <td><?= $inquilino->codigo_propietario ?></td>
                        <td><?= $recibo->unidad ?></td>
                    </tr>
                    <tr>
                        <th colspan="2">INQUILINO</th>
                        <th colspan="2">INMUEBLE</th>
                    </tr>
                    <tr>
                        <td colspan="2"><?= $inquilino->nombre ?> Contrato: <?= $inquilino->contrato ?></td>
                        <td colspan="2"><?= $recibo->inmueble ?></td>
                    </tr>
                    <tr>
                        <td><b>MES: </b> <?= $meses[$recibo->mes_emision-1] ?></td>
                        <td><b>AÑO: </b> <?= $recibo->ano_emision ?></td>
                        <td>
                            <div class="row">
                                <div class="col-xs-5"><b>TOTAL Bs.F</div><div class="col-xs-7" align="right"> <?= number_format($recibo->monto,2,',','.') ?></b></div>
                            </div></td>
                        <td>
                            <div class="row">
                                <div class="col-xs-5"><b>Bs</div><div class="col-xs-7" align="right"> <?= number_format($recibo->monto*1000,2,',','.') ?></b></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <center>Copia Web</center>
        </section>
        <script>
            window.print();
        </script>
    </body>
</html>
