<li>
    <a class="dropdown-toggle" href="#">
            <i class="menu-icon fa fa-lock"></i>
            <span class="menu-text">Admin</span>
            <b class="arrow fa fa-angle-down"></b>
    </a>
    <b class="arrow"></b>
    <ul class="submenu">
        <!--- Seguridad --->
        <li>
            <a class="dropdown-toggle" href="#">
                <i class="menu-icon fa fa-lock"></i>
                <span class="menu-text">Seguridad</span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class='submenu'>
                <li>
                    <a href="<?= base_url('seguridad/ajustes') ?>">
                        <i class="menu-icon fa fa-wrench"></i>
                        <span class="menu-text">Ajustes generales</span>                                    
                    </a>                                
                </li>                            
                <li>
                    <a href="<?= base_url('seguridad/grupos') ?>">
                        <i class="menu-icon fa fa-users"></i>
                        <span class="menu-text">Grupos</span>                                    
                    </a>                                
                </li>
                <li>
                    <a href="<?= base_url('seguridad/funciones') ?>">
                        <i class="menu-icon fa fa-arrows"></i>
                        <span class="menu-text">Funciones</span>                                    
                    </a>                                
                </li>
                <li>
                    <a href="<?= base_url('seguridad/user') ?>">
                        <i class="menu-icon fa fa-user"></i>
                        <span class="menu-text">Usuarios</span>                                    
                    </a>                                
                </li>                
                <li>
                    <a href="<?= base_url('seguridad/permisos') ?>">
                        <i class="menu-icon fa fa-key"></i>
                        <span class="menu-text">Permisos de unidades</span>                                    
                    </a>                                
                </li>
            </ul>
        </li>
        <!--- Fin seguridad ---->
         <!--- CMS --->
        <li>
            <a class="dropdown-toggle" href="#">
                <i class="menu-icon fa fa-file-o"></i>
                <span class="menu-text">CMS</span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class='submenu'>
                <li>
                    <a href="<?= base_url('cms/paginas') ?>">
                        <i class="menu-icon fa fa-file"></i>
                        <span class="menu-text">Paginas</span>                                    
                    </a>                                
                </li>
                <li>
                    <a href="<?= base_url('cms/link') ?>">
                        <i class="menu-icon fa fa-picture-o"></i>
                        <span class="menu-text">Links de interés</span>                                    
                    </a>                                
                </li>
                <li>
                    <a href="<?= base_url('cms/leyes') ?>">
                        <i class="menu-icon fa fa-bell"></i>
                        <span class="menu-text">Leyes</span>                                    
                    </a>                                
                </li> 
                <li>
                    <a href="<?= base_url('cms/boletines') ?>">
                        <i class="menu-icon fa fa-bell"></i>
                        <span class="menu-text">Boletines</span>                                    
                    </a>                                
                </li>
                <li>
                    <a href="<?= base_url('cms/inmueblesc') ?>">
                        <i class="menu-icon fa fa-bell"></i>
                        <span class="menu-text">Inmuebles</span>                                    
                    </a>                                
                </li>
                <li>
                    <a href="<?= base_url('cms/token') ?>">
                        <i class="menu-icon fa fa-bell"></i>
                        <span class="menu-text">Token</span>                                    
                    </a>                                
                </li>
                <li>
                    <a href="<?= base_url('cms/noticias') ?>">
                        <i class="menu-icon fa fa-bell"></i>
                        <span class="menu-text">Noticias</span>                                    
                    </a>                                
                </li>
                <li>
                    <a href="<?= base_url('cms/banner') ?>">
                        <i class="menu-icon fa fa-bell"></i>
                        <span class="menu-text">Banner</span>                                    
                    </a>                                
                </li>
            </ul>
        </li>
        <!--- Fin CMS ---->
        <!--- Tabalas Empresa --->
        <!--- Fin tablas Empresa ---->
        <!--- Tabalas maestras --->
        <li>
            <a class="dropdown-toggle" href="#">
                <i class="menu-icon fa fa-lock"></i>
                <span class="menu-text">Tablas Maestras</span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class='submenu'>
                <li>
                    <a href="<?= base_url('tablasmaestras/importar') ?>">
                        <i class="menu-icon fa fa-globe"></i>
                        <span class="menu-text">Importar</span>
                    </a>
                </li>
            </ul>
        </li>
        <!--- Fin tablas Maestras ---->        
    </ul>
</li>
<!--- Fin Admin ---->
