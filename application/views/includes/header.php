<header>
                <nav class="navbar navbar-default row" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header col-lg-2">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand visible-md" href="<?= site_url() ?>"><i class="glyphicon glyphicon-home"></i> YURUARY</a>
                    </div>
                    <div class="col-lg-13 collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav col-lg-12 row">
                            <li class="col-lg-2"><a href="<?= site_url() ?>"><i class="glyphicon glyphicon-home"></i> YURUARY</a></li>
                            <li class="col-lg-2"><a href="<?= site_url('p/quienes-somos') ?>"><i class="glyphicon glyphicon-user"></i> Quienes somos</a></li>
                            <li class="col-lg-2"><a href="<?= site_url('p/servicios') ?>"><i class="glyphicon glyphicon-briefcase"></i> Servicios</a></li>
                            <li class="col-lg-2"><a href="<?= site_url('in') ?>"><i class="glyphicon glyphicon-bookmark"></i> Inmuebles</a></li>
                            <li class="col-lg-2 dropdown" style="padding:0 0">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-book"></i> Documentación <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= site_url('v/link') ?>">Links de interes</a></li>
                                    <li><a href="<?= site_url('v/leyes') ?>">Descarga de leyes</a></li>
                                    <li><a href="<?= site_url('lista-de-noticias') ?>">Noticias</a></li>
                                </ul>
                            </li>
                            <li class="col-lg-2"><a href="<?= site_url('p/contactenos') ?>"><i class="glyphicon glyphicon-phone"></i> Contactenos</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav>
</header>
    <? if(!empty($_SESSION['user'])):  ?>
    <div align="right">
        <div class="dropdown" style="width:150px;">
            <a data-toggle="dropdown" href="#">
                <?= $_SESSION['nombre']." ".$_SESSION['apellido']."<br/> ".date("d/m/Y")." <span id='reloj'></span>" ?> <b class='caret'></b>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                <li><a href='<?= base_url('actualizar') ?>'>Actualizar Datos</a></li>
                <li><a href='<?= base_url('main/unlog') ?>'>Salir</a></li>
            </ul> 
        </div>
    </div>
    <? endif ?>
<script>
    $(document).ready(function(){
        update();
    });
    
    function update()
    {
        contador_h =<?= date("H") ?>;
        contador_s =<?= date("s") ?>;
        contador_m =<?= date("m") ?>;
	cronometro = setInterval(
        function(){
        if(contador_s==60)
        {
        contador_s=0;
        contador_m++;
	if(contador_m==60)
        {
            contador_m=0;
            contador_h++;
            if(contador_h>=24)
            {
                contador_h==0;
                document.location.refresh();
            }
        }
        }
        hora = contador_h;
        minuto = contador_m<10?'0'+contador_m:contador_m;
        segundo = contador_s<10?'0'+contador_s:contador_s;
	$("#reloj").html(hora+":"+minuto+":"+segundo);
        contador_s++;
        },1000);
    }
</script>