<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Permiso de condominios - EDIFICIOS</a></h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="panel-group" id="condominios">
                    <?php foreach($this->db->get('txt_edificios')->result() as $e): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading" <?= $this->db->get_where('permisos',array('user'=>$_SESSION['permiso_user'],'tipo'=>'condominio','edificio'=>$e->id))->num_rows>0?'style="background:lightblue"':'' ?>>
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#condominios" href="#<?= 'edificio'.$e->id ?>">
                            (<?= $e->nombre ?>)</a></h4>
                        </div>
                        <div id="<?= 'edificio'.$e->id ?>" class="panel-collapse collapse">
                        <div class="panel-body row"> 
                            <?php foreach($this->db->get_where('txt_propietarios_cond',array('edificio'=>$e->codigo))->result() as $u): ?>
                            <div class="col-xs-2"><input data-t='condominio' data-u="<?= $u->unidad ?>" data-p="<?= $e->codigo ?>" type="checkbox"><?= $u->unidad ?></div>                                    
                            <?php endforeach ?>
                        </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#collapseTwo input[type="checkbox"]').click(function(){
            var x = 't='+$(this).attr('data-t')+'&u='+$(this).attr('data-u')+'&p='+$(this).attr('data-p');
            if($(this).prop('checked'))
                ajax(x,true,'#loading',undefined,'<?= base_url('permisos/add') ?>');
            else
                ajax(x,true,'#loading',undefined,'<?= base_url('permisos/erase') ?>');
        });
    });
</script>