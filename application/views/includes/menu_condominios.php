<li>
    <a class="dropdown-toggle" href="#">
            <i class="menu-icon fa fa-building-o"></i>
            <span class="menu-text">Condominios</span>
            <b class="arrow fa fa-angle-down"></b>
    </a>
    <b class="arrow"></b>
    <ul class="submenu">
        <li>
            <a href="<?= base_url('condominios/recibos_condominios') ?>">
                <i class="menu-icon fa fa-building-o"></i>
                <span class="menu-text">Recibos</span>                                    
            </a>                                
        </li>                            
        <li>
            <a href="<?= base_url('condominios/cuadro_economico_financiero') ?>">
                <i class="menu-icon fa fa-users"></i>
                <span class="menu-text">Cuadro financiero</span>                                    
            </a>                                
        </li>
        <li>
            <a href="<?= base_url('condominios/deudor_condominios') ?>">
                <i class="menu-icon fa fa-legal"></i>
                <span class="menu-text">Fac/unidad</span>                                    
            </a>                                
        </li>
        <li>
            <a href="<?= base_url('condominios/deudor_condominios') ?>">
                <i class="menu-icon fa fa-legal"></i>
                <span class="menu-text">Fac/inmueble</span>                                    
            </a>                                
        </li>
    </ul>
</li>
<!--- Fin Admin ---->
