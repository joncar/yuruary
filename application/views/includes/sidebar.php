<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
            <?php $this->load->view('includes/menu_alquileres'); ?>            
            <?php $this->load->view('includes/menu_condominios'); ?>            
            <?php if($this->user->cuenta==99): ?>
            <?php $this->load->view('includes/menu_admin'); ?>
            <?php endif ?>
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
            try{
                ace.settings.check('sidebar' , 'collapsed');
                ace.settings.sidebar_collapsed(true, true);
            }catch(e){}
        </script>
</div>
<?php endif ?>
