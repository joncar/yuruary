<div class="row">
  
  <div class="col-xs-6 col-md-3">
    <a href="<?= base_url('empresa/empresacontratos/contratos') ?>" class="thumbnail well" style="text-align:center; padding:40px;">
      Contratos
    </a>
  </div>
    
  <div class="col-xs-6 col-md-3">
    <a href="<?= base_url('empresa/empresaliquidaciones/liquidaciones') ?>" class="thumbnail well" style="text-align:center; padding:40px;">
      Liquidaciones
    </a>
  </div>
    
  <div class="col-xs-6 col-md-3">
    <a href="<?= base_url('empresa/empresaferiados/feriado') ?>" class="thumbnail well" style="text-align:center; padding:40px;">
      Comprobante de feriados
    </a>
  </div>
    
  <div class="col-xs-6 col-md-3">
    <a href="<?= base_url('empresa/empresafiniquitos/finiquitos') ?>" class="thumbnail well" style="text-align:center; padding:40px;">
      Finiquitos
    </a>
  </div>
  
</div>