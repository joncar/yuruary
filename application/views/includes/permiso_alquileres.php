<div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Permiso de alquileres - PROPIETARIOS</a></h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="panel-group" id="alquileres">
                            <? $this->db->order_by('codigo','ASC') ?>
                            <? foreach($this->db->get('txt_propietarios')->result() as $pro): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" <?= $this->db->get_where('permisos',array('user'=>$_SESSION['permiso_user'],'tipo'=>'alquiler','edificio'=>$pro->codigo))->num_rows>0?'style="background:lightblue"':'' ?>>
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#alquileres" href="#<?= 'propietario'.$pro->id ?>">
                                    (<?= $pro->codigo ?>) - <?= $pro->denominacion ?></a></h4>
                                </div>
                                <div id="<?= 'propietario'.$pro->id ?>" class="panel-collapse collapse">
                                <div class="panel-body row">
                                    <? $inquilinos = $this->db->get_where('txt_inquilinos',array('codigo_propietario'=>$pro->codigo)) ?>
                                    <? if($inquilinos->num_rows>0): ?>
                                    <? foreach($inquilinos->result() as $in): ?>
                                    <div class="col-xs-2"><input data-t='alquiler' <?= $this->querys->getPublicAccess($_SESSION['permiso_user'],'alquiler',$pro->codigo,$in->codigo_unidad,$in->contrato)?'checked':'' ?> data-u="<?= $in->codigo_unidad ?>" data-p="<?= $pro->codigo ?>" data-c='<?= $in->contrato ?>' type="checkbox"><?= $in->codigo_unidad ?> (Contrato: <?= $in->contrato ?>)</div>
                                    <? endforeach ?>
                                    <? else: ?>
                                    <div class="col-xs-4"><input data-t='alquiler' <?= $this->querys->getPublicAccess($_SESSION['permiso_user'],'alquiler',$pro->codigo,'','','')?'checked':'' ?> data-u="" data-p="<?= $pro->codigo ?>" data-c='' type="checkbox">Asignar propiedad</div>
                                    <? endif ?>
                                </div>
                                </div>
                            </div>
                            <? endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script>
    $(document).ready(function(){
        $('#collapseOne input[type="checkbox"]').click(function(){
            var x = 't='+$(this).attr('data-t')+'&u='+$(this).attr('data-u')+'&p='+$(this).attr('data-p')+"&c="+$(this).attr('data-c');
            if($(this).prop('checked'))
                ajax(x,true,'#loading',undefined,'<?= base_url('permisos/add') ?>');
            else
                ajax(x,true,'#loading',undefined,'<?= base_url('permisos/erase') ?>');
        });
    });
</script>