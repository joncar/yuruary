<li>
    <a class="dropdown-toggle" href="#">
            <i class="menu-icon fa fa-building-o"></i>
            <span class="menu-text">Alquileres</span>
            <b class="arrow fa fa-angle-down"></b>
    </a>
    <b class="arrow"></b>
    <ul class="submenu">
        <li>
            <a href="<?= base_url('alquileres/recibos_alquileres') ?>">
                <i class="menu-icon fa fa-building-o"></i>
                <span class="menu-text">Recibos</span>                                    
            </a>                                
        </li>                            
        <li>
            <a href="<?= base_url('alquileres/estado_cuenta') ?>">
                <i class="menu-icon fa fa-users"></i>
                <span class="menu-text">Estado de cuenta</span>                                    
            </a>                                
        </li>
        <li>
            <a href="<?= base_url('alquileres/deudor') ?>">
                <i class="menu-icon fa fa-legal"></i>
                <span class="menu-text">Deudor</span>                                    
            </a>                                
        </li>
    </ul>
</li>
<!--- Fin Admin ---->
