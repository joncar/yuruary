<!Doctype html>
<html lang="es">
	<head>
		<title>Administradora Yuruary C.A.</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
		<script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script src="<?= base_url('assets/frame.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">
		<?php 
		if(!empty($css_files) && !empty($js_files)):
		foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
		<?php endforeach; ?>
		<?php foreach($js_files as $file): ?>
		<script src="<?= $file ?>"></script>
		<?php endforeach; endif; ?>
                
                <!-- Respond.js proxy on external server -->
	</head>
	<body>            
            <section class="container">
            <?php $this->load->view('includes/header'); ?>
            <?php $this->load->view($view) ?>
            <?php $this->load->view('includes/footer') ?>
            </section>
            <script src="<?= base_url('assets/respond.js') ?>"></script>
        </body>
</html>