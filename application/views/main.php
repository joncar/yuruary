<? $this->load->view('includes/subheader'); ?>
<article>
                    <div class="row">
                    <div class="col-sm-6">
                        <div class="dividir col-sm-12 "><h4>Administradora Yuruary C.A</h4></div>
                        <?= substr($this->db->get_where('paginas',array('url'=>'quienes-somos'))->row()->texto,0,400)."... <a href='".site_url('p/quienes-somos')."'>Leer más</a>" ?>
                        <div class="dividir col-sm-12 "><h4>Noticias</h4></div>
                        <? $this->db->limit('3'); $noticias = $this->querys->getNoticias(); ?>
                        <? if($noticias->num_rows>0) $noticias->row()->texto.= '<p align="right"><a href="'.site_url('lista-de-noticias').'">Leer todas</a></p>'; ?>
                        <? $this->load->view('includes/noticias',array('data'=>$noticias)) ?>
                    </div>
                    <div class="col-sm-6">
                        <div class="dividir col-sm-13 "><h4>Pago de servicios</h4></div>
                        <div>
                            <div align="justify">
                                <p>Administradora Yuruary CA. le ofrece a sus clientes,  el servicio de pago en nuestras oficinas, a través de tarjeta de debito y cheque; contamos además con el servicio de cobradores en su edificio.</p>
                                <p>En los Banco Provincial y Banesco, Banca Universal puede realizar pagos en línea a través de Internet</p>
                                <p>En todas las agencias del Banco  Provincial  puede formalizar sus pagos con su Aviso de Cobro.</p>
                                <p>
                                <a href="https://mispagos.provincial.com/empresa/yuruary"><?= img('img/provincial.png','width:32%',TRUE,'class="img-thumbnail"') ?></a>
                                <a href="http://www.banesco.com/"><?= img('img/banesco.png','width:32%;',TRUE,'class="img-thumbnail"') ?></a></p>
                            </div>
                            <form role="form" method='post' onsubmit='return validar(this)' action='<?= base_url('main/regbol') ?>' class="form-horizontal well"  >
                                <? if(!empty($msj2))echo $msj2 ?>
                                <h3>Subscribete a nuestro boletin diario</h3>
                              <?= input('email','Email','email') ?>
                              <div align="center"><button type="submit" class="btn btn-success">Subscribir</button></div>
                            </form>
                            <h3>Nuestras redes sociales</h3>
                            <div class="row">
                                <?=  $this->querys->get_social('twitter'); ?>
                                <?=  $this->querys->get_social('facebook'); ?>
                                <?=  $this->querys->get_social('youtube'); ?>
                                <?=  $this->querys->get_social('google'); ?>
                            </div>
                        </div>
                    </div>
        </div>
</article>