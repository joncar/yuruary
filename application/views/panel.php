<div class="page-header">
        <h1>
                Escritorio
                <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <a href="<?= base_url('panel/seleccionarUnidad') ?>" title="Seleccionar unidad"><?php if(!empty($this->user->unidad)) echo '- '.$this->user->unidadName.' <span style="color:blue">Cambiar unidad</span>'; else echo 'Unidad no seleccionada' ?></a>
                </small>
        </h1>
</div><!-- /.page-header -->

<div class="row">
        <div class="col-xs-12">
               <?= empty($crud)?'':$this->load->view('cruds/'.$crud) ?>
        </div><!-- /.col -->
</div><!-- /.row -->
