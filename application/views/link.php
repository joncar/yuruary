<? $this->load->view('includes/subheader'); ?>
<? $links = $this->querys->get_links() ?>
<? if($links): ?>
<? 
$contenido = '<div class="row">';
foreach($links->result() as $l):
    $contenido.= '<div class="col-lg-2">'.
        $this->load->view('predesign/tumbnail',array('img'=>'files/'.$l->foto,'title'=>'<center>'.$l->titulo.'</center>','content'=>'<center><a href="'.$l->url.'" class="btn btn-success">Visitar</a></center>'),TRUE).
        '</div>';
endforeach;
$contenido .= '</div>';
?>
<? $this->load->view('includes/struct_page',array('title'=>'Links de interes','content'=>$contenido)) ?>
<? else: ?>
<? $this->load->view('includes/struct_page',array('title'=>'Descarga de leyes','content'=>'Lo sentimos pero por ahora no tenemos links de interes')) ?>
<? endif; ?>