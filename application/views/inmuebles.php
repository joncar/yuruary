<?php
if(empty($_SESSION['token']))
{
    header("Location:".base_url('in'));
}
?>
<section class="row">
    <aside class="col-lg-3 well">
        <b>Filtro de busqueda</b>
        <div>
        <? 
            $data = array(''=>'Seleccione');
            foreach($this->db->get('localidad')->result() as $c)
            $data[$c->id] = $c->nombre;
            $selected = empty($_GET['ciudad'])?'0':$_GET['ciudad'];
            echo "<div class='row'><div class='col-lg-4'>Ciudad: </div><div class='col-lg-8'>".form_dropdown('fciudad',$data,$selected,'id="fciudad"').'</div></div>';
        ?>
        </div>
        <div>
        <? 
            $data = array(''=>'Seleccione');
            foreach($this->db->get('operaciones')->result() as $c)
            $data[$c->id] = $c->nombre;
            $selected = empty($_GET['operacion'])?'0':$_GET['operacion'];
            echo "<div class='row'><div class='col-lg-4'>Operacion: </div><div class='col-lg-8'>".form_dropdown('foperaciones',$data,$selected,'id="foperaciones"').'</div></div>';
        ?>
        </div>
        <div>
        <? 
            $data = array(''=>'Seleccione');
            foreach($this->db->get('tipos')->result() as $c)
            $data[$c->id] = $c->nombre;
            $selected = empty($_GET['tipo'])?'0':$_GET['tipo'];
            echo "<div class='row'><div class='col-lg-4'>Tipo: </div><div class='col-lg-8'>".form_dropdown('ftipos',$data,$selected,'id="ftipos"').'</div></div>';
        ?>
        </div>
        <div>
        <? 
            $data = array(''=>'Seleccione','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');
            $selected = empty($_GET['hab'])?'0':$_GET['hab'];
            echo "<div class='row'><div class='col-lg-4'>Habitaciones: </div><div class='col-lg-8'>".form_dropdown('fhab',$data,$selected,'id="fhab"').'</div></div>';
        ?>
        </div>
        <div>
        <? 
            $data = array(''=>'Seleccione','0'=>'0','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');
            $selected = empty($_GET['est'])?'0':$_GET['est'];
            echo "<div class='row'><div class='col-lg-4' title=\"Puestos de estacionamiento\">P. Est: </div><div class='col-lg-8'>".form_dropdown('fest',$data,$selected,'id="fest"').'</div></div>';
        ?>
        </div>
        <div>
        <? 
            $data = array(''=>'Seleccione','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');
            $selected = empty($_GET['banos'])?'0':$_GET['banos'];
            echo "<div class='row'><div class='col-lg-4' title=\"Puestos de estacionamiento\">Baños: </div><div class='col-lg-8'>".form_dropdown('fbanos',$data,$selected,'id="fbanos"').'</div></div>';
        ?>
        </div>
        <div>
            <b>Por precio</b>
            <div class="row">
                <form action="">
                <? $selected = empty($_GET['minprecio'])?'':$_GET['minprecio']; ?>   
                <div class="col-sm-6"><?= form_input('text',$selected,'placeholder="Min" id="fminprecio" class="form-control"') ?></div>
                <? $selected = empty($_GET['maxprecio'])?'':$_GET['maxprecio']; ?> 
                <div class="col-sm-6"><?= form_input('text',$selected,'placeholder="Max" id="fmaxprecio" class="form-control"') ?></div>
                </form>
            </div>
        </div>
    </aside>
    <article class="col-lg-9">
        <? $data = 
            array('collapse'=>
                array('form'=>
                    array('action'=>'','method'=>'get','id'=>'formsearch','button'=>'Buscar','items'=>
                    array(
                        form_input('search',!empty($_GET['search'])?$_GET['search']:'','class="form-control" placeholder="Buscar"'),
                        '<input type="hidden" name="ciudad" id="ciudad">',
                        '<input type="hidden" name="operacion" id="operacion">',
                        '<input type="hidden" name="tipo" id="tipo">',
                        '<input type="hidden" name="minprecio" id="minprecio">',
                        '<input type="hidden" name="maxprecio" id="maxprecio">',
                        '<input type="hidden" name="hab" id="hab">',
                        '<input type="hidden" name="est" id="est">',
                        '<input type="hidden" name="banos" id="banos">'))
                    )) ?>
        <? $this->load->view('predesign/navbar',array('data'=>$data));?>
        <? $this->load->view('includes/listinmuebles',array('data'=>$this->querys->getInmuebles())) ?>
    </article>
</section>
<script>
        function sub()
        {
            $("#ciudad").val($("#fciudad").val());
            $("#operacion").val($("#foperaciones").val());
            $("#tipo").val($("#ftipos").val());
            $("#minprecio").val($("#fminprecio").val());
            $("#maxprecio").val($("#fmaxprecio").val());
            $("#hab").val($("#fhab").val());
            $("#est").val($("#fest").val());
            $("#banos").val($("#fbanos").val());
            $("#formsearch").submit();
        }
        $("#fciudad,#foperaciones,#ftipos,#fminprecio,#fmaxprecio,#fhab,#fest,#fbanos").change(function(){sub();});
</script>