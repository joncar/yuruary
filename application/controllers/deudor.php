<?php
require_once('panel.php');
class Deudor extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(empty($_SESSION['user']))
                header("Location:".base_url());
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('txt_deudores');
            $crud->set_subject('Recibos');
            $crud->columns('inmueble','unidad','ano_emision','mes_emision','monto');
            
            $permiso = $this->db->get_where('permisos',array('user'=>$_SESSION['user']));
            if($permiso->num_rows==0)
                $crud->where("contrato",-1);
            $or = 0;
            foreach($permiso->result() as $p)
            {
                if($or==0)$crud->where("contrato",$p->contrato);
                else $crud->or_where("contrato",$p->contrato);
                $or = 1;
            }
            //Fields
            
            //unsets
           
            $crud->unset_delete();
            $crud->unset_edit();
            $crud->unset_add();
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_read();
            //Displays
            
            //Fields types
            
            //Validations
            
            //Callbacks
            $crud->callback_column('codigo_unidad',array($this,'enlace'));
            $crud->callback_column('monto',array($this,'monto'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'deudor';
            $this->loadView($output);
        }
        
        function acciones($val,$row)
        {
            return '<a href="javascript:imprimir(\''.$row->id.'\')" title="Imprimir recibo"><i class="glyphicon glyphicon-print"></i></a>';
        }
        
        function enlace($val,$row)
        {
            return $this->db->get_where('txt_deudores',array('contrato'=>$row->contrato))->num_rows>0?'<a href="'.base_url('deudor/mostrar/'.$row->contrato).'">'.$val.'</a>':$val;
        }
        
        function imprimir()
        {
                $permiso = $this->db->get_where('permisos',array('user'=>$_SESSION['user']));
                $or = 0;
                foreach($permiso->result() as $p)
                {
                    if($or==0)$this->db->where("contrato",$p->contrato);
                    else $this->db->or_where("contrato",$p->contrato);
                    $or = 1;
                }
                $this->db->order_by('inmueble');
                $data = $this->db->get('txt_deudores');
                if($data->num_rows>0){
                 $this->load->view('reportes/deudor',array('deudor'=>$data));   
                }
                else
                $this->load->view('404');
        }
}

?>