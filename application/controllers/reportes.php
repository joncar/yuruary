<?php
require_once('panel.php');
class Reportes extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(empty($_SESSION['user']))
                header("Location:".base_url());
	}
        public function cuadro()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('txt_edificios');            
            $crud->set_subject('Recibos');
            $x = 0;
            //Fields
            
            //unsets
            $crud->columns('codigo','nombre');
            $crud->unset_delete();
            $crud->unset_edit();
            $crud->unset_add();
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_read();
            //Displays
            
            //Fields types
            
            //Validations
            
            //Callbacks
            $crud->callback_column('codigo',function($val,$row){
                if(get_instance()->db->get_where('txt_cuadro_economico',array('edificio'=>$row->codigo))->num_rows>0)
                return '<a href="'.base_url('reportes/cuadro2/'.$row->codigo).'">'.$val.'</a>';
                else return $val;
            });
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'recibos';
            $this->loadView($output);
        }
        
        function cuadro2($edificio,$x = '',$y = '',$z = ''){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('txt_cuadro_economico');            
            $crud->set_subject('Reportes');
            $crud->where('edificio',$edificio);            
            $x = 0;
            //Fields
            
            //unsets           
            $crud->columns('conjunto','edificio','mes','saldo_inmueble','acciones');
            $crud->unset_delete();
            $crud->unset_edit();
            $crud->unset_add();
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_read();
            //Displays
            $crud->callback_column('acciones',function($val,$row){
                return '<a href="javascript:imprimir(\''.$row->id.'\')" title="Imprimir estado economico"><i class="glyphicon glyphicon-print"></i></a>';
            });
            //Fields types
            
            //Validations
            
            //Callbacks            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'cuadro_economico';
            $this->loadView($output);
        }
        
        function imprimir_cuadro($id)
        {
            if(empty($id))
            $this->load->view('404');
            else
            {
                $data = $this->db->get_where('txt_cuadro_economico',array('id'=>$id));
                if($data->num_rows>0){
                 $data = $data->row();                 
                 $edificio = $this->db->get_where('txt_edificios',array('codigo'=>$data->edificio));                                  
                 $this->load->view('reportes/cuadro_economico',array('recibo'=>$data,'edificio'=>$edificio->row()));   
                }
                else
                $this->load->view('404');
            }
        }

}

?>