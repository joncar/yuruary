<?php
require_once('panel.php');
class Permisos extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('tablas'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
             $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');
            //Fields
            $crud->columns('usuario','nombre','permiso');
            //unsets
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_read()
                 ->unset_edit()
                 ->unset_export()
                 ->unset_print();
            //Displays
            
            //Fields types

            //Validations
            
            
            //Callbacks
            $crud->callback_column('permiso',array($this,'__permiso'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'usuarios';
            $this->loadView($output);
        }   
        
        function __permiso($val,$row)
        {
            return '<a href="'.base_url('permisos/permiso/'.$row->id).'">Otorgar permiso</a>';
        }
        public function permiso($id)
        {
            if($id>0) 
            {
                $_SESSION['permiso_user'] = $id; 
                $this->loadView('permiso');
            }
            else
                header("Location".base_url('permisos'));
        }
        
        function erase()
        {
            $this->form_validation->set_rules('t','t','required');
            $this->form_validation->set_rules('p','p','required');
            if($this->form_validation->run() && !empty($_SESSION['permiso_user']))
            {
                $tipo = $this->input->post('t',TRUE);
                $unidad = $this->input->post('u',TRUE);
                $contrato = $this->input->post('c',TRUE);
                $edificio = $this->input->post('p',TRUE);
                $user = $_SESSION['permiso_user'];
                
                $this->db->where('tipo',$tipo);
                $this->db->where('unidad',$unidad);
                $this->db->where('edificio',$edificio);
                $this->db->where('user',$user);
                $this->db->where('contrato',$contrato);
                $this->db->delete('permisos');
            }
            else
                $this->loadView('404');
        }
        function add()
        {
            $this->form_validation->set_rules('t','t','required');
            $this->form_validation->set_rules('p','p','required');
            if($this->form_validation->run() && !empty($_SESSION['permiso_user']))
            {
                $tipo = $this->input->post('t',TRUE);
                $unidad = $this->input->post('u',TRUE);
                $edificio = $this->input->post('p',TRUE);
                $user = $_SESSION['permiso_user'];
                $contrato = $this->input->post('c',TRUE);
                $data = array(
                    'tipo'=>$tipo,
                    'unidad'=>$unidad,
                    'edificio'=>$edificio,
                    'user'=>$user,
                    'contrato'=>$contrato
                );
                if($this->db->get_where('permisos',$data)->num_rows==0)
                $this->db->insert('permisos',$data);
                else
                $this->erase();
            }
        }
}
?>