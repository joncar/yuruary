<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
	public function __construct()
	{
            parent::__construct();                        
            if (empty($_SESSION['user'])){
                header("Location:" . base_url('registro/index/add?redirect=' . $_SERVER['REQUEST_URI']));
            }
            else {                
                if(empty($this->user->unidad) && $this->router->fetch_class()!=='permisos' &&  $this->router->fetch_method()!='seleccionarUnidad'){
                    header("Location:".base_url('panel/seleccionarUnidad'));
                }
            }            
            $this->load->library('ajax_grocery_CRUD');
            $this->load->library('html2pdf/html2pdf');           
            ini_set('display_errors', true);
	}
        
        protected function crud_function($x,$y){            
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            $crud->unset_fields('id');
            $requireds = array();
            foreach($this->db->field_data($table) as $row){
                array_push($requireds,$row->name);
            }
            $crud->required_fields_array($requireds);            
            if(method_exists('panel',$table)){
                $crud = call_user_func(array('panel',$table),$crud,$x,$y);
            }            
            return $crud;
        }
        
        public function index()
	{
		$this->loadView('panel');
	}
        
        public function loadView($param = array('view'=>'panel'))
        {
            if(empty($_SESSION['user'])){
                header("Location:".base_url('main?redirect='.$_SERVER['REQUEST_URI']));
            }
            if(!$this->user->hasAccess()){
                throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operación','403');
            }
            else{
                if(!empty($param->output)){
                    $param->view = empty($data->view)?'panel':$data->view;
                    $param->crud = empty($data->crud)?'user':$data->crud;
                    $param->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
                }
                elseif(is_string($param)){
                    $param = array('view'=>$param);
                }
                $this->load->view('templateadmin',$param);
            }
        }
        
        function monto($val)
        {
            return '<div align="right">'.$val.'</div>';
        }
        
        function seleccionarUnidad($x = '',$y = '',$nombre = ''){            
            if(!empty($x)){
                $this->user->setVariable('unidad',$x);
                $this->user->setVariable('edificio',$y);
                $this->user->setVariable('unidadName',strtoupper(str_replace('-',' ',$nombre)));
                redirect('panel');
            }
            else{
                $crud = new ajax_grocery_CRUD();
                $crud->set_table('permisos');
                $crud->set_theme('bootstrap2');
                $crud->set_subject('Selecciona una unidad para administrarla');
                
                
                $empresas = $this->db->get_where('permisos',array('permisos.user'=>$this->user->id));
                if($empresas->num_rows==0){
                    $crud->where('permisos.id','-1');
                }
                foreach($empresas->result() as $p){
                    $crud->or_where('permisos.id',$p->id);
                }
                $crud->unset_add()
                        ->unset_edit()
                        ->unset_delete()
                        ->unset_print()
                        ->unset_export()
                        ->unset_read();
                
                $crud->columns('edificios','tipo','unidad');
                $crud->unset_searchs = array('edificios');
                $crud->callback_column('edificios',function($val,$row){
                    if($row->tipo=='alquiler'){
                        $ed = get_instance()->db->get_where('txt_propietarios',array('codigo'=>$row->edificio));
                        $den = $ed->num_rows>0?$ed->row()->denominacion:'No encontrado';
                    }else{
                        $ed = get_instance()->db->get_where('txt_propietarios_cond',array('edificio'=>$row->edificio));
                        $den = $ed->num_rows>0?$ed->row()->nombre:'No encontrado';
                    }
                    return $den;
                });
                $crud->callback_column('unidad',function($val,$row){
                    return '<a href="'.base_url('panel/seleccionarUnidad/'.$row->unidad.'/'.$row->edificio.'/'.toURL($row->edificios)).'">'.$val.'</a>';
                });
                $crud = $crud->render();
                $this->loadView($crud);
            }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */