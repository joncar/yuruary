<?php
require_once('panel.php');
class Estado_cuenta extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(empty($_SESSION['user']))
                header("Location:".base_url());
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('txt_estado_cuentas');
            $crud->set_subject('Recibos');
            
            
            $permiso = $this->db->get_where('permisos',array('user'=>$_SESSION['user']));
            if($permiso->num_rows==0)
                $crud->where("codigo_propietario",-1);
            $or = 0;
            foreach($permiso->result() as $p)
            {
                if($or==0)$crud->where("codigo_propietario",$p->edificio);
                else $crud->or_where("codigo_propietario",$p->edificio);
                $or = 1;
            }
                
            //Fields
            
            //unsets
            
            $crud->unset_delete();
            $crud->unset_edit();
            $crud->unset_add();
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_read();
            //Displays
            $crud->columns('codigo_propietario','Señor(es)','denominacion','ano_cuenta','mes_cuenta','Acciones');
            $crud->display_as('codigo_propietario','Cuenta Nº')
                 ->display_as('mes_cuenta','Mes')
                 ->display_as('ano_cuenta','Año')
                 ->display_as('denominacion','Inmueble administrado');
            //Fields types
            
            //Validations
            
            //Callbacks
            $crud->callback_column('Acciones',array($this,'acciones'));
            $crud->callback_column('Señor(es)',array($this,'Sres'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'estado_cuenta';
            $this->loadView($output);
        }
        function Sres($val,$row)
        {
            return $this->db->get_where('txt_propietarios',array('codigo'=>$row->codigo_propietario))->row()->denominacion;
        }
        function acciones($val,$row)
        {
            $liquidacion = $this->db->get_where('txt_liquidacion',array('codigo_inmueble'=>$row->codigo_inmueble,'codigo_propietario'=>$row->codigo_propietario,'mes_cuenta'=>$row->mes_cuenta,'ano_cuenta'=>$row->ano_cuenta));
            
            $str = '<a href="javascript:imprimir(\''.$row->id.'\')" title="Imprimir solo estado de cuenta"><i class="glyphicon glyphicon-print"></i></a>';
            if($liquidacion->num_rows>0)
            {
                $str.= '<a target="_new" href="'.base_url('liquidacion/imprimir/'.$liquidacion->row()->id).'" title="Imprimir liquidacion de propietarios"><i class="glyphicon glyphicon-hand-down"></i></a>';
                $str.= '<a href="javascript:imprimir2(\''.$row->id.'\',\''.$liquidacion->row()->id.'\')" title="Imprimir liquidacion y estado de cuenta"><i class="glyphicon glyphicon-hand-down"></i></a>';
            }
            
            return $str;
        }
        
        function imprimir($id)
        {
            if(empty($id))
            $this->load->view('404');
            else
            {
                $data = $this->db->get_where('txt_estado_cuentas',array('id'=>$id));
                if($data->num_rows>0){
                 $data = $data->row();
                 $detalle = $this->db->get_where('txt_estado_cuentas_detalles',array('codigo_propietario'=>$data->codigo_propietario,'mes_cuenta'=>$data->mes_cuenta,'ano_cuenta'=>$data->ano_cuenta));
                 $this->load->view('reportes/estado_cuenta',array('estado'=>$data,'detalle'=>$detalle));   
                }
                else
                $this->load->view('404');
            }
        }
}

?>