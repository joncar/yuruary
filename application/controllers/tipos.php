<?php
require_once('panel.php');
class Tipos extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('tablas'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('tipos');
            $crud->set_subject('Tipos');
            //Fields
            
            //unsets
            
            //Displays
            
            //Fields types
            
            //Validations
            $crud->required_fields('nombre');
            //Callbacks
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'tablas';
            $this->loadView($output);
        }
}

?>