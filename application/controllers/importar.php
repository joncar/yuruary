<?php
require_once('panel.php');
class Importar extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('importar'))
                header("Location:".base_url('panel'));
            $this->db->query("SET NAMES 'utf8'");
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('importar');
            $crud->set_subject('archivo');
            //Fields
            
            //unsets
            
            //Displays
            
            //Fields types
            $crud->set_field_upload('archivo','files');
            $crud->field_type('descripcion','enum',
            array(
                'ALQUILERES - ARCHIVO DE PROPIETARIOS',
                'ALQUILERES - ARCHIVO DE UNIDADES',
                'ALQUILERES - ARCHIVO DE INQUILINOS',
                'ALQUILERES - ARCHIVO DE RECIBOS',
                'ALQUILERES - ARCHIVO DE DETALLE DE RECIBOS',
                'ALQUILERES - ARCHIVO DE RECIBOS COBRADOS',
                'ALQUILERES - ARCHIVO DE DEUDORES DE ALQUILER',
                'ALQUILERES - ARCHIVO DE DETALLE DE DEUDORES DE ALQUILER',
                'ALQUILERES - ARCHIVO DE ESTADO DE CUENTAS',
                'ALQUILERES - ARCHIVO DE DETALLE DE ESTADO DE CUENTAS',
                'ALQUILERES - ARCHIVO DE LIQUIDACION DE PROPIETARIOS',
                'ALQUILERES - ARCHIVO DE DETALLE DE LIQUIDACION DE PROPIETARIOS',
                'CONDOMINIO - ARCHIVO DE EDIFICIOS',
                'CONDOMINIO - ARCHIVO DE PROPIETARIOS',
                'CONDOMINIO - RECIBOS',
                'CUADRO ECONOMICO FINANCIERO'));
            $crud->field_type('status','invisible');
            //Validations
            $crud->required_fields('archivo','descripcion');
            //Callbacks
            $crud->callback_before_insert(array($this,'binsertion'));
            $crud->callback_before_update(array($this,'binsertion'));
            $crud->columns('archivo','descripcion','procesar');
            $crud->callback_column('procesar',function($val,$row){
                return '<a target="_new" href="'.base_url('importar/'.$this->procesar($row->descripcion).'/'.strip_tags($row->archivo)).'">Procesar archivo</a>';
            });
            $crud->callback_before_upload(array($this,'valarchivo'));
            
            if(!empty($_POST['descripcion']) && !empty($_POST['archivo']))
                $crud->set_lang_string ('insert_success_message','Archivo guardado con exito <script>window.open("'.base_url('importar/'.$this->procesar($_POST['descripcion']).'/'.$_POST['archivo']).'")</script>');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'usuarios';
            $this->loadView($output);
        }
       
        
        function valarchivo($file,$info)
        {
            if($file[$info->encrypted_field_name]['type']=='text/plain')
            {
                return true;
            }
            else
                return 'Lo siento pero el archivo permitido debe ser en formato .txt '.$file[$info->encrypted_field_name]['type'];
        }
        
        function binsertion($post)
        {
            
            //$post['status'] = $this->procesar($post['archivo'],$post['descripcion']);
            return $post;
        }
        
        function procesar($tipo)
        {
            switch($tipo)
            {
                case 'ALQUILERES - ARCHIVO DE PROPIETARIOS': return 'propietarios'; break;
                case 'ALQUILERES - ARCHIVO DE UNIDADES': return 'unidades'; break;
                case 'ALQUILERES - ARCHIVO DE INQUILINOS': return 'inquilinos'; break;
                case 'ALQUILERES - ARCHIVO DE RECIBOS': return 'recibos'; break;
                case 'ALQUILERES - ARCHIVO DE DETALLE DE RECIBOS': return 'recibos_detalles'; break;
                case 'ALQUILERES - ARCHIVO DE RECIBOS COBRADOS': return 'recibos_cobrados'; break;
                case 'ALQUILERES - ARCHIVO DE DEUDORES DE ALQUILER': return 'deudores'; break;
                case 'ALQUILERES - ARCHIVO DE DETALLE DE DEUDORES DE ALQUILER': return 'deudores_detalles'; break;
                case 'ALQUILERES - ARCHIVO DE ESTADO DE CUENTAS': return 'estado_cuentas'; break;
                case 'ALQUILERES - ARCHIVO DE DETALLE DE ESTADO DE CUENTAS': return 'estado_cuentas_detalles'; break;
                case 'ALQUILERES - ARCHIVO DE LIQUIDACION DE PROPIETARIOS': return 'liquidacion'; break;
                case 'ALQUILERES - ARCHIVO DE DETALLE DE LIQUIDACION DE PROPIETARIOS': return 'liquidacion_detalles'; break;
                case 'CONDOMINIO - ARCHIVO DE EDIFICIOS':return 'edificios'; break;
                case 'CONDOMINIO - ARCHIVO DE PROPIETARIOS':return 'propietarios_cond'; break;
                case 'CONDOMINIO - RECIBOS': return 'recibos_condominios'; break;
                case 'CUADRO ECONOMICO FINANCIERO': return 'cuadro_economico_financiero'; break;
            }
        }
        
        
        /**************** Importaciones ***************/
        function propietarios($archivo)
        {
            $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
            //Output a line of the file until the end is reached
            $x = 0;
            $y = 0;
            
            while(!feof($file))
            {
                $linea = explode("|",fgets($file));
                if(count($linea)!=3)
                $y++;
                $data['codigo'] = $linea[0];
                $data['denominacion'] = !empty($linea[1])?$linea[1]:'';
                $data['direccion'] = !empty($linea[2])?$linea[2]:'';
                if($this->db->get_where('txt_propietarios',array('codigo'=>$linea[0]))->num_rows==0){
                $this->db->insert('txt_propietarios',$data);
                $x++;
                    echo '<div style="color:blue">Se agrego el propietario '.$data['denominacion'].'</div>';
                    flush();
                }
                else{
                    $this->db->update('txt_propietarios',$data,array('codigo'=>$linea[0]));
                    echo '<div style="color:green">Se modifico el propietario '.$data['denominacion'].'</div>';
                    flush();
                }
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros ";

            print_f($linea);
                flush();
        }
        
        function unidades($archivo)
        {
            $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
             $x = 0;
            $y = 0;
            while(!feof($file))
            {
                $linea = explode("|",fgets($file));
                if(count($linea)!=3)continue;
                $y++;
                $data['codigo_propietario'] = $linea[0];
                $data['codigo_unidad'] = $linea[1];
                if($this->db->get_where('txt_unidades',array('codigo_propietario'=>$linea[0],'codigo_unidad'=>$linea[1]))->num_rows==0){
                $this->db->insert('txt_unidades',$data);
                 $x++;
                 echo '<div style="color:blue">Se agrego la unidad '.$data['codigo_unidad'].'</div>';
                    flush();
                }
                else
                {
                    $x++;
                    $this->db->where(array('codigo_propietario'=>$linea[0],'codigo_unidad'=>$linea[1]));
                    $this->db->update('txt_unidades',$data);
                    echo '<div style="color:green">Se modifico la unidad '.$data['codigo_unidad'].'</div>';
                    flush();
                }
            }   
            fclose($file);
           echo 'Se agregaron '.$x." de ".$y." registros";
        }
        
        function inquilinos($archivo)
        {
            $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
             $x = 0;
            $y = 0;
            while(!feof($file))
            {
                $linea = explode("|",fgets($file));
                
                if(count($linea)!=6)continue;
                $y++;
                $data['codigo_propietario'] = $linea[0];
                $data['codigo_unidad'] = $linea[1];
                $data['nombre'] = $linea[2];
                $data['codigo'] = $linea[3];
                $data['contrato'] = $linea[4];
                if($this->db->get_where('txt_inquilinos',array('codigo_propietario'=>$linea[0],'codigo_unidad'=>$linea[1],'codigo'=>$linea[3]))->num_rows==0){
                $this->db->insert('txt_inquilinos',$data);
                    $x++;
                    echo '<div style="color:blue">Se agrego el inquilino '.$data['nombre'].'</div>';
                    flush();
                }
                else
                {
                    $this->db->where(array('codigo_propietario'=>$linea[0],'codigo_unidad'=>$linea[1],'codigo'=>$linea[3]));
                    $this->db->update('txt_inquilinos',$data);
                    echo '<div style="color:green">Se modifico el inquilino '.$data['nombre'].'</div>';
                    flush();
                    $x++;
                }
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros";
        }
        
        function recibos($archivo)
        {
            $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
             $x = 0;
            $y = 0;
            while(!feof($file))
            {
                
                $linea = explode("|",fgets($file));
                if(count($linea)!=11)continue;
                $y++;
                $data['contrato'] = $linea[0];
                $data['inmueble'] = $linea[1];
                $data['unidad'] = $linea[2];
                $data['recibo'] = $linea[3];
                $data['ano_emision'] = $linea[4];
                $data['mes_emision'] = $linea[5];
                $data['tipo'] = $linea[6];
                $data['direccion'] = $linea[7];
                $data['monto_domicilio'] = $linea[8];
                $data['monto'] = $linea[9];
                if($this->db->get_where('txt_recibos',array('contrato'=>$linea[0],'ano_emision'=>$linea[4],'mes_emision'=>$linea[5],'unidad'=>$linea[2]))->num_rows==0){
                $this->db->insert('txt_recibos',$data);
                $x++;
                echo '<div style="color:blue">Se agrego el recibo '.$data['recibo'].'</div>';
                    flush();
                }
                else
                {
                    $x++;
                    $this->db->where(array('contrato'=>$linea[0],'ano_emision'=>$linea[4],'mes_emision'=>$linea[5],'unidad'=>$linea[2]));
                    $this->db->update('txt_recibos',$data);
                    echo '<div style="color:green">Se modifico el recibo '.$data['recibo'].'</div>';
                    flush();
                }
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros";
        }
        
        function recibos_detalles($archivo)
        {
            $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
             $x = 0;
            $y = 0;
            while(!feof($file))
            { 
                $linea = explode("|",fgets($file));
                if(count($linea)!=6)continue;
                $y++;
                $data['contrato'] = $linea[0];
                $data['recibo'] = $linea[1];
                $data['codigo_concepto'] = $linea[2];
                $data['denominacion_concepto'] = $linea[3];
                $data['monto'] = $linea[4];
                if($this->db->get_where('txt_recibos_detalles',array('contrato'=>$linea[0],'recibo'=>$linea[1],'codigo_concepto'=>$linea[2]))->num_rows==0){
                $this->db->insert('txt_recibos_detalles',$data);
                 $x++;
                 echo '<div style="color:blue">Se agrego el recibo '.$data['recibo'].'</div>';
                    flush();
                }
                else
                {
                    $x++;
                    $this->db->where(array('contrato'=>$linea[0],'recibo'=>$linea[1],'codigo_concepto'=>$linea[2]));
                    $this->db->update('txt_recibos_detalles',$data);
                    echo '<div style="color:green">Se modifico el recibo '.$data['recibo'].'</div>';
                    flush();
                }
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros";
        }
        
        function recibos_cobrados($archivo)
        {
            $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
            $x = 0;
            $y = 0;
            while(!feof($file))
            { 
                $linea = explode("|",fgets($file));
                if(count($linea)!=5)continue;
                $y++;
                $data['contrato'] = $linea[0];
                $data['recibo'] = $linea[1];
                $data['detalle'] = $linea[2];
                $data['fecha'] = date("Y-m-d",strtotime($linea[3]));
                if($this->db->get_where('txt_recibos_cobrados',array('contrato'=>$linea[0],'recibo'=>$linea[1],'fecha'=>$linea[3]))->num_rows==0){
                $this->db->insert('txt_recibos_cobrados',$data);
                 $x++;
                 echo '<div style="color:blue">Se agrego el recibo '.$data['recibo'].'</div>';
                    flush();
                }
                else
                {
                    $x++;
                    $this->db->where(array('contrato'=>$linea[0],'recibo'=>$linea[1],'fecha'=>$linea[3]));
                    $this->db->update('txt_recibos_cobrados',$data);
                    echo '<div style="color:green">Se modifico el recibo '.$data['recibo'].'</div>';
                    flush();
                }
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros";
        }
        
        function deudores($archivo)
        {
            $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
            $x = 0;
            $y = 0;
            while(!feof($file))
            { 
                $linea = explode("|",fgets($file));
                if(count($linea)!=11)continue;
                $y++;
                $data['contrato'] = $linea[0];
                $data['inmueble'] = htmlspecialchars($linea[1]);
                $data['unidad'] = $linea[2];
                $data['recibo'] = $linea[3];
                $data['ano_emision'] = $linea[4];
                $data['mes_emision'] = $linea[5];
                $data['tipo'] = $linea[6];
                $data['direccion'] = htmlspecialchars($linea[7]);
                $data['monto_domicilio'] = $linea[8];
                $data['monto'] = $linea[9];
                if($this->db->get_where('txt_deudores',array('contrato'=>$linea[0],'ano_emision'=>$linea[4],'mes_emision'=>$linea[5],'unidad'=>$linea[2]))->num_rows==0){
                $this->db->insert('txt_deudores',$data);
                 $x++;
                 echo '<div style="color:blue">Se agrego el deudor '.$data['recibo'].'</div>';
                    flush();
                }
                else
                {
                    $x++;
                    $this->db->where(array('contrato'=>$linea[0],'ano_emision'=>$linea[4],'mes_emision'=>$linea[5],'unidad'=>$linea[2]));
                    $this->db->update('txt_deudores',$data);
                    echo '<div style="color:green">Se modifico el deudor '.$data['recibo'].'</div>';
                    flush();
                }
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros";
        }
        
        function deudores_detalles($archivo)
        {
             $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
             $x = 0;
            $y = 0;
            while(!feof($file))
            { 
                $linea = explode("|",fgets($file));
                if(count($linea)!=6)continue;
                $y++;
                $data['contrato'] = $linea[0];
                $data['recibo'] = $linea[1];
                $data['codigo_concepto'] = $linea[2];
                $data['denominacion_concepto'] = $linea[3];
                $data['monto'] = $linea[4];                
                if($this->db->get_where('txt_deudores_detalles',array('contrato'=>$linea[0],'recibo'=>$linea[1],'codigo_concepto'=>$linea[2]))->num_rows==0){
                $this->db->insert('txt_deudores_detalles',$data);
                 $x++;
                 echo '<div style="color:blue">Se agrego el deudor '.$data['recibo'].'</div>';
                 flush();
                }
                else
                {
                    $x++;
                    $this->db->where(array('contrato'=>$linea[0],'recibo'=>$linea[1],'codigo_concepto'=>$linea[2]));
                    $this->db->update('txt_deudores_detalles',$data);
                    echo '<div style="color:green">Se modifico el deudor '.$data['recibo'].'</div>';
                    flush();
                }
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros";
        }
        
        function estado_cuentas($archivo)
        {
             $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
             $x = 0;
            $y = 0;
            while(!feof($file))
            { 
                $linea = explode("|",fgets($file));
                if(count($linea)!=9)continue;
                $y++;
                $data['codigo_inmueble'] = $linea[0];
                $data['codigo_propietario'] = $linea[1];
                $data['ano_cuenta'] = $linea[2];
                $data['mes_cuenta'] = $linea[3];
                $data['denominacion'] = htmlspecialchars($linea[4]);
                $data['direccion'] = htmlspecialchars($linea[5]);
                $data['porcentaje'] = $linea[6];
                $data['formas_pagos'] = $linea[7];
                if($this->db->get_where('txt_estado_cuentas',array('codigo_inmueble'=>$linea[0],'codigo_propietario'=>$linea[1],'ano_cuenta'=>$linea[2],'mes_cuenta'=>$linea[3]))->num_rows==0){
                $this->db->insert('txt_estado_cuentas',$data);
                 $x++;
                 echo '<div style="color:blue">Se agrego el estado de cuenta '.$data['codigo_propietario'].'</div>';
                    flush();
                }
                else
                {
                    $x++;
                    $this->db->where(array('codigo_inmueble'=>$linea[0],'codigo_propietario'=>$linea[1],'ano_cuenta'=>$linea[2],'mes_cuenta'=>$linea[3]));
                    $this->db->update('txt_estado_cuentas',$data);
                    echo '<div style="color:green">Se agrego el estado de cuenta '.$data['codigo_propietario'].'</div>';
                    flush();
                }                
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros";
        }
        
        function estado_cuentas_detalles($archivo)
        {
             $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
             $x = 0;
            $y = 0;
            while(!feof($file))
            { 
                $linea = explode("|",fgets($file));
                $y++;
                if(count($linea)!=11)continue;
                $data['codigo_inmueble'] = $linea[0];
                $data['codigo_propietario'] = $linea[1];
                $data['ano_cuenta'] = $linea[2];
                $data['mes_cuenta'] = $linea[3];
                $data['linea'] = $linea[4];
                $data['dia'] = $linea[5];
                $data['concepto'] = htmlspecialchars($linea[6]);
                $data['debe'] = $linea[7];
                $data['haber'] = $linea[8];
                $data['saldo'] = $linea[9];
                if($this->db->get_where('txt_estado_cuentas_detalles',array('codigo_inmueble'=>$linea[0],'codigo_propietario'=>$linea[1],'linea'=>$linea[4]))->num_rows==0){
                $this->db->insert('txt_estado_cuentas_detalles',$data);
                 $x++;
                 echo '<div style="color:blue">Se agrego el estado de cuenta '.$data['codigo_propietario'].'</div>';
                 flush();
                }
                else
                {
                    $x++;
                    $this->db->where(array('codigo_inmueble'=>$linea[0],'codigo_propietario'=>$linea[1],'linea'=>$linea[4]));
                    $this->db->update('txt_estado_cuentas_detalles',$data);
                    echo '<div style="color:green">Se agrego el estado de cuenta '.$data['codigo_propietario'].'</div>';
                    flush();
                }     
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros";
        }
        
        function liquidacion($archivo)
        {
             $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
             $x = 0;
            $y = 0;
            while(!feof($file))
            { 
                $linea = explode("|",fgets($file));
                if(count($linea)!=7)continue;
                $y++;
                $data['codigo_inmueble'] = $linea[0];
                $data['codigo_propietario'] = $linea[1];
                $data['ano_cuenta'] = $linea[2];
                $data['mes_cuenta'] = $linea[3];
                $data['denominacion'] = $linea[4];
                $data['monto'] = $linea[5];
                if($this->db->get_where('txt_liquidacion',array('codigo_inmueble'=>$linea[0],'codigo_propietario'=>$linea[1],'ano_cuenta'=>$linea[2],'mes_cuenta'=>$linea[3]))->num_rows==0){
                $this->db->insert('txt_liquidacion',$data);
                 $x++;
                 echo '<div style="color:blue">Se agrego la liquidacion '.$data['denominacion'].'</div>';
                 flush();
                }
                else
                {
                    $x++;
                    $this->db->where(array('codigo_inmueble'=>$linea[0],'codigo_propietario'=>$linea[1],'ano_cuenta'=>$linea[2],'mes_cuenta'=>$linea[3]));
                    $this->db->update('txt_liquidacion',$data);
                    echo '<div style="color:green">Se modifico la liquidacion '.$data['denominacion'].'</div>';
                    flush();
                }  
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros";
        }
        
        function liquidacion_detalles($archivo)
        {
             $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
             $x = 0;
            $y = 0;
            while(!feof($file))
            { 
                $linea = explode("|",fgets($file));
                if(count($linea)!=11)continue;
                $y++;
                $data['codigo_inmueble'] = $linea[0];
                $data['codigo_propietario'] = $linea[1];
                $data['ano_cuenta'] = $linea[2];
                $data['mes_cuenta'] = $linea[3];
                $data['denominacion'] = $linea[4];
                $data['linea'] = $linea[5];
                $data['concepto'] = $linea[7];
                $data['debe'] = $linea[8];
                $data['haber'] = $linea[9];
                $data['saldo'] = $linea[10];
                if($this->db->get_where('txt_liquidacion_detalles',array('codigo_inmueble'=>$linea[0],'codigo_propietario'=>$linea[1],'ano_cuenta'=>$linea[2],'mes_cuenta'=>$linea[3],'linea'=>$linea[5]))->num_rows==0){
                $this->db->insert('txt_liquidacion_detalles',$data);
                 $x++;
                 echo '<div style="color:blue">Se agrego la liquidacion '.$data['denominacion'].'</div>';
                 flush();
                }
                else
                {
                    $x++;
                    $this->db->where(array('codigo_inmueble'=>$linea[0],'codigo_propietario'=>$linea[1],'ano_cuenta'=>$linea[2],'mes_cuenta'=>$linea[3],'linea'=>$linea[5]));
                    $this->db->update('txt_liquidacion_detalles',$data);
                    echo '<div style="color:green">Se modifico la liquidacion '.$data['denominacion'].'</div>';
                    flush();
                }  
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros";
        }
        
        function edificios($archivo)
        {
            $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
            $x = 0;
            $y = 0;
            while(!feof($file))
            {
                $linea = fgets($file);
                $carro = 0;
                $codigo = trim(substr($linea, $carro, $carro+6)); $carro+=6;
                $nombre = trim(substr($linea,$carro,$carro+35)); $carro+=35;
                $conjunto = trim(substr($linea,$carro,$carro+2)); $carro+=2;
                $direccion = trim(substr($linea,$carro,$carro+68)); $carro+=68;
                $contrato = trim(substr($linea,$carro,$carro+4)); $carro+=4;
                
                $data = array('codigo'=>$codigo,'nombre'=>$nombre,'conjunto'=>$conjunto,'direccion'=>$direccion,'contrato'=>$contrato);
                if($this->db->get_where('txt_edificios',array('codigo'=>$codigo))->num_rows==0)
                {
                    $this->db->insert('txt_edificios',$data);
                    $x++;
                    echo '<div style="color:blue">Agregado '.$codigo.'</div>';                    
                    flush();                    
                }
                else
                {
                    $this->db->where('codigo',$codigo);
                    $this->db->update('txt_edificios',$data);
                    echo '<div style="color:green">Modificado '.$codigo.'</div>';                    
                    flush();                    
                    $x++;
                }
                $y++;
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros <script>window.close();</script>";
        }
        
        function propietarios_cond($archivo)
        {
            $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;
            $x = 0;
            $y = 0;
            while(!feof($file))
            {
                $linea = fgets($file);
                $carro = 0;
                
                $conjunto = trim(substr($linea, $carro, $carro+2)); $carro+=2;
                $edificio = trim(substr($linea,$carro,$carro+6)); $carro+=6;
                $unidad = trim(substr($linea,$carro,$carro+5)); $carro+=7;
                $nombre = trim(substr($linea,$carro,$carro+51)); $carro+=51;
                
                $val = array('conjunto'=>$conjunto,'edificio'=>$edificio,'unidad'=>$unidad);
                $data = $val;
                $data['nombre'] = $nombre;
                if($this->db->get_where('txt_propietarios_cond',$val)->num_rows==0)
                {
                    $this->db->insert('txt_propietarios_cond',$data);
                    echo '<div style="color:blue">Se agrego el propietario '.$conjunto.' '.$edificio.' '.$unidad.' '.$nombre.'</div>';
                    flush();     
                    $x++;
                }
                else
                {
                    $this->db->update('txt_propietarios_cond',$data,$val);
                    echo '<div style="color:green">Se modifico el propietario '.$nombre.'</div>';
                    flush();
                    $x++;
                }
                $y++;
            }   
            fclose($file);
            echo 'Se agregaron '.$x." de ".$y." registros <script>window.close();</script>";
        }
        
        function recibos_condominios($archivo)
        {
            $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;                        
            //Si algun dia leen esto quiero que sepan que este algoritmo solo es valido para recibos en los cuales cada una de sus secciones midan lo mismo de alto
            $recibos = array();
            $hojas = 0;
            $lineas = 0;
            //Dividir los recibos por hojas. Cada index de recibo es una hoja, y dentro hay arrays con las lineas
            while(!feof($file)){
                if($lineas==0)
                    $recibos[$hojas] = array();
                $recibos[$hojas][$lineas] = fgets($file);
                $lineas++;
                if($lineas>81){$lineas = 0; $hojas++;}
            }
            //cantidad de recibos 33 para el ejemplo en el que se creo
            $fechas = array();
            $alicuotas = array();
            $netos = array();
            $conceptos = array();
            $conjuntos = array();
            $inmuebles = array();
            $unidades = array();
            $totalconjunto = array();
            $totalinmueble = array();
            $totalunidad = array();
            $edificios = array();
            $apartamentos = array();
            $clientes = array();
            $observaciones = array();
            //fondos
            $fondos = array();
            $saldosanteriores = array();
            $cuotasdelmes = array();
            $cargos = array();
            $saldos = array();
            //Codigos
            $codedificio = array();
            $codapartamento = array();
            $codcliente = array();
            $codzona = array();
            $codruta = array();
            foreach($recibos as $n=>$r){
                if(!empty($r[10])){
                //header Linea 11
                $fecha = substr($r[10],46,10);
                if(trim($fecha)!=='')array_push($fechas,trim($fecha));
                $alicuota = substr($r[10],67,11);
                if(trim($alicuota)!=='')array_push($alicuotas,trim($alicuota));
                $neto = substr($r[10],100,10);
                if(trim($neto)!=='')array_push($netos,trim($neto));

                //cuerpo Linea 15 - 56
                $conceptos[$n] = array();
                $conjuntos[$n] = array();
                $inmuebles[$n] = array();
                $unidades[$n] = array();        
                for($i=14;$i<55;$i++)
                {
                    $concepto = trim(substr($r[$i],1,63));
                    if(!empty($concepto)){//Si se entera el sistema de que esta es la ultima linea rompe el ciclo
                    array_push($conceptos[$n],trim($concepto));
                    $conjunto = substr($r[$i],64,4);                   
                    array_push($conjuntos[$n],trim($conjunto));
                    $inmueble = substr($r[$i],69,20);
                    array_push($inmuebles[$n],trim($inmueble));
                    $unidad = substr($r[$i],88,20);
                    array_push($unidades[$n],trim($unidad));
                    }                    
                }
                //Linea 56
                $totalconjunto[$n] = trim(substr($r[55],60,10));                
                $totalinmueble[$n] = trim(substr($r[55],69,20));
                $totalunidad[$n] = trim(substr($r[55],88,20));                         
                //Linea 58 - 63
                $observacion = '';
                for($i=57;$i<63;$i++)
                {
                    $observacion.=trim(substr($r[$i],1,100)).'<br/>';                        
                }        
                array_push($observaciones,$observacion);                
                //Linea 64
                $identificador[$n] = trim(substr($r[63],48,12));                
                $nrorecibo[$n] = trim(substr($r[63],65,15));
                //fondos         
                $fondos[$n] = array();
                $saldosanteriores[$n] = array();
                $cuotasdelmes[$n] = array();
                $cargos[$n] = array();
                $saldos[$n] = array();        
                for($i=69;$i<72;$i++)
                {            
                    array_push($fondos[$n],trim(substr($r[$i],1,34)));
                    array_push($saldosanteriores[$n],trim(substr($r[$i],35,15)));
                    array_push($cuotasdelmes[$n],trim(substr($r[$i],51,15)));
                    array_push($cargos[$n],trim(substr($r[$i],72,15)));
                    array_push($saldos[$n],trim(substr($r[$i],93,20)));            
                }
                $codconjunto[$n] = trim(substr($r[76],71,3));
                $codedificio[$n] = trim(substr($r[76],73,6));
                $codunidad[$n] = trim(substr($r[76],81,10));
                $codzona[$n] = trim(substr($r[76],97,7));
                $codruta[$n] = trim(substr($r[76],110,8));
                $vence[$n] = trim(substr($r[80],81,15));                
                $vence[$n] = date("Y-m-d",strtotime(str_replace("/","-",$vence[$n])));
                //Comenzar a guardar por recibo...
                $data = array(                    
                    'nro_recibo'=>$nrorecibo[$n],
                    'conjunto'=>$codconjunto[$n],
                    'edificio'=>$codedificio[$n],
                    'unidad'=>$codunidad[$n],
                    'propietario'=>trim(substr($r[6],1)),
                    'mes_ano'=>$fecha,
                    'alicuota'=>$alicuota,
                    'pendientes'=>trim(substr($r[66],54,3)),
                    'total_deuda'=>trim(substr($r[66],87,23)),
                    'neto'=>$neto,
                    'total_conjunto'=>$totalconjunto[$n],
                    'total_inmueble'=>$totalinmueble[$n],
                    'total_unidad'=>$totalunidad[$n],
                    'observaciones'=>$observacion,
                    'vencimiento'=>$vence[$n],
                    'identificacion_cliente'=>$identificador[$n]
                );
                if($this->db->get_where('txt_recibos_cond',array('edificio'=>$codedificio[$n],'unidad'=>$codunidad[$n]))->num_rows>0){
                    $this->db->delete('txt_recibos_cond',array('edificio'=>$codedificio[$n],'unidad'=>$codunidad[$n]));
                }
                $this->db->insert('txt_recibos_cond',$data);
                $id = $this->db->insert_id();
                foreach($conceptos[$n] as $l=>$c)
                {
                    $data = array(                    
                    'recibo'=>$id,
                    'detalle'=>$conceptos[$n][$l],
                    'conjunto'=>$conjuntos[$n][$l],
                    'inmueble'=>$inmuebles[$n][$l],
                    'unidad'=>$unidades[$n][$l]
                    );
                    $this->db->insert('txt_recibos_cond_detalles',$data);
                }
                
                foreach($fondos[$n] as $l=>$c)
                { 
                    $data = array(                    
                    'recibo'=>$id,
                    'fondos'=>$fondos[$n][$l],
                    'saldo_anterior'=>$saldosanteriores[$n][$l],
                    'cuota_mes'=>$cuotasdelmes[$n][$l],
                    'cargoabono'=>$cargos[$n][$l],
                    'saldo_actual'=>$saldos[$n][$l]                 
                    );
                    $this->db->insert('fondos_condominio',$data);
                } 
                echo '<div style="color:blue">Se agrego el recibo '.$nrorecibo[$n].'</div>';
                flush(); 
              }
            }
            fclose($file);
            echo "Se agregaron ".$n." registros";
        }
        
        function cuadro_economico_financiero($archivo){
            $file = fopen(base_url("files/".$archivo), "r");
            if(!$file)return false;                        
            //Si algun dia leen esto quiero que sepan que este algoritmo solo es valido para recibos en los cuales cada una de sus secciones midan lo mismo de alto
            $recibos = array();
            $hojas = 0;
            $lineas = 0;
            //Dividir los recibos por hojas. Cada index de recibo es una hoja, y dentro hay arrays con las lineas
            while(!feof($file)){
                if($lineas==0)
                    $recibos[$hojas] = array();
                $recibos[$hojas][$lineas] = fgets($file);
                $lineas++;
                if($lineas>23){$lineas = 0; $hojas++;}
            }
            //cantidad de recibos 33 para el ejemplo en el que se creo
            if(!empty($recibos)){                
                foreach($recibos as $n=>$r){
                    if(!empty($r[1])){
                        $data = array();
                        $data['fecha'] = substr($r[1],strpos($r[1],'Fecha.:')+8,8);
                        list($dia,$mes,$ano) = explode("/",$data['fecha']);
                        $data['fecha'] = ($ano+2000).'-'.$mes.'-'.$dia;                    
                        $data['hora'] = substr($r[2],strpos($r[2],'Hora..:')+8,8);
                        $data['conjunto'] = substr($r[6],strpos($r[6],'Conjunto: ')+10,2);
                        $data['conjunto_nombre'] = substr($r[6],strpos($r[6],'Conjunto: ')+13);
                        $data['edificio'] = substr($r[8],strpos($r[8],'Inmueble: ')+12,4);
                        $data['mes'] = substr($r[10],strpos($r[10],'al Mes :')+12,7);
                        $data['saldo'] = trim(str_replace(",",".",str_replace(".","",str_replace("CR","",substr($r[12],strpos($r[12],'Saldo de Fondos al Mes')+33)))));
                        $data['deuda'] = trim(str_replace(",",".",str_replace(".","",str_replace("CR","",substr($r[14],strpos($r[14],'Deuda Condominio al Mes')+34)))));
                        $data['fondo'] = trim(str_replace(",",".",str_replace(".","",str_replace("CR","",substr($r[16],strpos($r[16],'Fondo de Operacion')+29)))));
                        $data['pagos'] = trim(str_replace(",",".",str_replace(".","",str_replace("CR","",substr($r[18],strpos($r[18],'Pagos contra los Fondos')+34)))));
                        $data['saldo_inmueble'] = trim(str_replace(",",".",str_replace(".","",str_replace("CR","",substr($r[20],strpos($r[20],'Saldo del Inmueble')+29)))));
                        $data['rendimiento'] = trim(str_replace(",",".",str_replace(".","",str_replace("CR","",substr($r[22],strpos($r[22],'Rendimiento')+12)))));
                        $data['financiamiento'] = trim(str_replace("+","",str_replace(",",".",str_replace(".","",str_replace("CR","",substr($r[23],strpos($r[23],'financiamiento')+25))))));
                        
                        $where = array('conjunto'=>$data['conjunto'],'fecha'=>$data['fecha'],'edificio'=>$data['edificio']);
                        if($this->db->get_where('txt_cuadro_economico',$where)->num_rows==0){
                        $this->db->insert('txt_cuadro_economico',$data);
                        echo '<div style="color:blue">Se agrego conjunto: '.$data['conjunto'].' Edificio: '.$data['edificio'].' fecha: '.$data['fecha'].'</div>';
                        }
                        else{
                            $this->db->update('txt_cuadro_economico',$data,$where);
                            echo '<div style="color:green">Se modifico conjunto: '.$data['conjunto'].' Edificio: '.$data['edificio'].' fecha: '.$data['fecha'].'</div>';
                        }                        
                        flush();     
                    }
                }
            }
            fclose($file);
            echo "Se agregaron ".$n." registros";
        }
        
        /*function recibos_condominios($archivo)
        {
            $file = fopen(base_url('files/'.$archivo), "r");
            if(!$file)return false;                        
            //Si algun dia leen esto quiero que sepan que este algoritmo solo es valido para recibos en los cuales cada una de sus secciones midan lo mismo de alto
            $recibos = array();
            $hojas = 0;
            $lineas = 0;
            //Dividir los recibos por hojas. Cada index de recibo es una hoja, y dentro hay arrays con las lineas
            while(!feof($file)){
                if($lineas==0)
                    $recibos[$hojas] = array();
                $recibos[$hojas][$lineas] = fgets($file);
                if($lineas > 0 && strpos('Conjunto:',$recibos[$hojas][$lineas])>0){$lineas = 0; $hojas++;}
                $lineas++;                                                               
            }
            print_r($recibos[0]);
            fclose($file);
        }*/
}

?>
