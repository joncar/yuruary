<?php
require_once('panel.php');
class Liquidacion extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(empty($_SESSION['user']))
                header("Location:".base_url());
	}
        public function index()
        {
            
        }
        
        function acciones($val,$row)
        {
            return '<a href="javascript:imprimir(\''.$row->id.'\')" title="Imprimir estado de cuenta"><i class="glyphicon glyphicon-print"></i></a>';
        }
        
        function imprimir($id)
        {
            if(empty($id))
            $this->load->view('404');
            else
            {
                $data = $this->db->get_where('txt_liquidacion',array('id'=>$id));
                if($data->num_rows>0){
                 $data = $data->row();
                 $detalle = $this->db->get_where('txt_liquidacion_detalles',array('codigo_propietario'=>$data->codigo_propietario,'mes_cuenta'=>$data->mes_cuenta,'ano_cuenta'=>$data->ano_cuenta));
                 $this->load->view('reportes/liquidacion',array('liquidacion'=>$data,'detalle'=>$detalle));   
                }
                else
                $this->load->view('404');
            }
        }
}

?>