<?php
require_once('panel.php');
class Token extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('token'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('token');
            $crud->set_subject('Token');
            //Fields
            
            //unsets

            //Displays
            $crud->display_as('pass','Token');
            
            //Fields types
            
            
            //Validations
            $crud->required_fields('email','pass');
            $crud->set_rules('email','Email','required|valid_email|is_unique[token.email]');
            $crud->set_rules('pass','Token','required|min_length[8]|max_length[16]');
            //Callbacks
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'usuarios';
            $this->loadView($output);
        }
}

?>