<?php
require_once('panel.php');
class Ajustes extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('ajustes'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('ajustes');
            $crud->set_subject('Ajustes');
            //Fields
            
            //unsets
            $crud->unset_delete();
            $crud->unset_add();
            //Displays
            
            //Fields types
            $crud->field_type('formato_fecha','enum',array('d-m-Y','d/m/Y','Y-m-d','Y/m/d'));
            //Validations
            $crud->required_fields('moneda','formato_fecha');
            $crud->set_rules('facebook','Facebook','valid_url');
            $crud->set_rules('twitter','Twitter','valid_url');
            $crud->set_rules('google','Google','valid_url');
            $crud->set_rules('youtube','Youtube','valid_url');
            //Callbacks
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'usuarios';
            $this->loadView($output);
        }
}

?>