<?php
require_once APPPATH.'/controllers/panel.php';    
class Recibos_condominios extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(empty($_SESSION['user']))
                header("Location:".base_url());
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('txt_propietarios_cond');
            if($_SESSION['cuenta']!=99)$crud->set_model('recibos_condominio_model');
            $crud->set_subject('Recibos');
            $x = 0;
            //Fields
            
            //unsets
            $crud->columns('edificio','unidad');
            $crud->unset_delete();
            $crud->unset_edit();
            $crud->unset_add();
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_read();
            //Displays
            
            //Fields types
            
            //Validations
            
            //Callbacks
            $crud->callback_column('unidad',function($val,$row){
                if(get_instance()->db->get_where('txt_recibos_cond',array('edificio'=>$row->edificio,'unidad'=>$val))->num_rows>0)
                return '<a href="'.base_url('recibos_condominios/recibos/'.$row->edificio.'/'.$val).'">'.$val.'</a>';
                else return $val;
            });
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'recibos';
            $this->loadView($output);
        }
        
        function recibos($edificio,$unidad,$x = '',$y = '',$z = ''){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('txt_recibos_cond');            
            $crud->set_subject('Recibos');
            $crud->where('edificio',$edificio);
            $crud->where('unidad',$unidad);
            $x = 0;
            //Fields
            
            //unsets
            $crud->columns('edificio','unidad','mes_ano','acciones');
            $crud->unset_delete();
            $crud->unset_edit();
            $crud->unset_add();
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_read();
            //Displays
            $crud->callback_column('acciones',function($val,$row){
                return '<a href="javascript:imprimir(\''.$row->id.'\')" title="Imprimir recibo"><i class="glyphicon glyphicon-print"></i></a>';
            });
            //Fields types
            
            //Validations
            
            //Callbacks            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'recibos_condominio';
            $this->loadView($output);
        }
        
        function imprimir($id)
        {
            if(empty($id))
            $this->load->view('404');
            else
            {
                $data = $this->db->get_where('txt_recibos_cond',array('id'=>$id));
                if($data->num_rows>0){
                 $data = $data->row();
                 $detalle = $this->db->get_where('txt_recibos_cond_detalles',array('recibo'=>$data->id));                 
                 $edificio = $this->db->get_where('txt_edificios',array('codigo'=>$data->edificio));                 
                 $fondos = $this->db->get_where('fondos_condominio',array('recibo'=>$data->id));                 
                 $this->load->view('reportes/recibos_condominio',array('recibo'=>$data,'detalle'=>$detalle,'edificio'=>$edificio->row(),'fondos'=>$fondos));   
                }
                else
                $this->load->view('404');
            }
        }

}

?>