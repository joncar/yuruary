<?php
require_once('panel.php');
class Recibos extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(empty($_SESSION['user']))
                header("Location:".base_url());
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('txt_inquilinos');
            $crud->set_model('recibos_alquileres_model');
            $crud->set_subject('Recibos');
            $x = 0;
            //Fields
            
            //unsets
            $crud->columns('Inmueble','Unidad','Inquilino','codigo','contrato');
            $crud->unset_delete();
            $crud->unset_edit();
            $crud->unset_add();
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_read();
            //Displays
            
            //Fields types
            
            //Validations
            
            //Callbacks
            $crud->callback_column('Inmueble',array($this,'inmueble'));
            $crud->callback_column('Unidad',array($this,'enlace'));
            $crud->callback_column('Inquilino',array($this,'inquilino'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'recibos';
            $this->loadView($output);
        }
        function inmueble($val,$row)
        {
            $inmueble = $this->db->get_where('txt_recibos',array('contrato'=>$row->contrato));
            $inmueble = $inmueble->num_rows>0?$inmueble->row()->inmueble:'N/E';
            return $inmueble;
        }
        function enlace($val,$row)
        {
            $unidad = $this->db->get_where('txt_recibos',array('contrato'=>$row->contrato));
            $unidad = $unidad->num_rows>0?$unidad->row()->unidad:$row->codigo_unidad;
            return 
            $this->db->get_where('txt_recibos',array('contrato'=>$row->contrato))->num_rows>0?'<a href="'.base_url('recibos/mostrar/'.$row->contrato).'">'.$unidad.'</a>':$unidad;
        }
        
        function unidad($val,$row)
        {
            $inmueble = $this->db->get_where('txt_recibos',array('contrato'=>$row->contrato));
            $inmueble = $inmueble->num_rows>0?$inmueble->row()->inmueble:'N/E';
            return $inmueble;
        }
        
        function inquilino($val,$row)
        {
            $inmueble = $this->db->get_where('txt_inquilinos',array('contrato'=>$row->contrato));
            $inmueble = $inmueble->num_rows>0?$inmueble->row()->nombre:'N/E';
            return $inmueble;
        }
        
        function mesTransform($val)
        {
            $mes = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            return $mes[$val-1];
        }
        
        function mostrar($id = '')
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('txt_recibos');
            $crud->set_subject('Recibos');
            $crud->where('contrato',$id);
            //Fields
            
            //unsets
            $crud->unset_delete();
            $crud->unset_edit();
            $crud->unset_add();
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_read();
            //Displays
            $crud->columns('ano_emision','mes_emision','monto','acciones');
            //Fields types
            
            //Validations
            
            //Callbacks
            $crud->callback_column('mes_emision',array($this,'mesTransform'));
            $crud->callback_column('acciones',array($this,'acciones'));
            $crud->callback_column('monto',array($this,'monto'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'recibos2';
            $output->contrato = $id;
            $this->loadView($output);
        }
        
        function acciones($val,$row)
        {
            return '<a href="javascript:imprimir(\''.$row->id.'\')" title="Imprimir recibo"><i class="glyphicon glyphicon-print"></i></a>';
        }
        
        function imprimir($id)
        {
            if(empty($id))
            $this->load->view('404');
            else
            {
                $data = $this->db->get_where('txt_recibos',array('id'=>$id));
                if($data->num_rows>0){
                 $data = $data->row();
                 $detalle = $this->db->get_where('txt_recibos_detalles',array('recibo'=>$data->recibo));
                 $inquilino = $this->db->get_where('txt_inquilinos',array('contrato'=>$data->contrato))->row();
                 $this->load->view('reportes/recibo',array('recibo'=>$data,'detalle'=>$detalle,'inquilino'=>$inquilino));   
                }
                else
                $this->load->view('404');
            }
        }
}

?>